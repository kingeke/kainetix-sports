<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class About extends Model
{
    use HasSlug;

    protected $fillable = ['order', 'title', 'description'];

    public function getSlugOptions() : SlugOptions{
        return SlugOptions::create()->generateSlugsFrom('title')->saveSlugsTo('slug');
    }
    
}
