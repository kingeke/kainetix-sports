<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookedEvent extends Model
{

    protected $fillable = ['event_id', 'email', 'name', 'number'];

    public function event(){
        return $this->belongsTo('App\Event');
    }
}
