<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $fillable = ['name', 'email', 'comment', 'post_id', 'parent_id', 'child_id'];

    public function post(){
        return $this->belongsTo('App\Post');
    }
    
    public function replies(){
        return $this->hasMany('App\Comment', 'parent_id');
    }
    
    public function user(){
        return $this->belongsTo('App\Comment', 'child_id');
    }

    public function delete(){
        
        // delete all related photos 
        $this->replies()->delete();

        //delete the event
        return parent::delete();
    }
}
