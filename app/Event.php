<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Event extends Model
{
    use HasSlug;

    protected $dates = ['time'];

    protected $fillable = ['title', 'time', 'venue', 'description', 'image', 'bookings'];

    public function getSlugOptions() : SlugOptions{
        return SlugOptions::create()->generateSlugsFrom('title')->saveSlugsTo('slug');
    }

    public function participants(){
        return $this->hasMany('App\BookedEvent');
    }
    
    public function delete(){
        
        // delete all related photos 
        $this->participants()->delete();

        //delete the event
        return parent::delete();
    }
}
