<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Expertise extends Model
{
    use HasSlug;

    protected $fillable = ['order', 'title', 'description', 'image'];

    public function getSlugOptions() : SlugOptions{
        return SlugOptions::create()->generateSlugsFrom('title')->saveSlugsTo('slug');
    }
    
}
