<?php

namespace App\Http\Controllers;

use App\{Admin, Event, Gallery, Post, Video, Talent};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Validator, Auth};
use Session;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Carbon\Carbon;
//traits
use App\Http\Traits\{WebsiteTraits, AdminTraits, MediaTraits, TalentTraits, CustomTraits};

class AdminController extends Controller{
    
    use WebsiteTraits, AdminTraits, MediaTraits, TalentTraits, CustomTraits, SendsPasswordResetEmails;

    private $admin;

    public function __construct(){
        
        $this->middleware(function ($request, $next) {

            if(Auth::guard('admins')->user()){

                $this->admin = Auth::guard('admins')->user(); 

                view()->share([
                    'admin' => $this->admin
                ]);
            }   
            
            else{
                Auth::guard('admins')->logout();
            }

            return $next($request);
        });
    }
    
    //index
    public function index(){

        $events = Event::latest()->with('participants')->paginate(20);

        $media = Gallery::count() + Video::count();

        $posts = Post::latest()->with('comments')->paginate(20);

        $talents = Talent::latest()->paginate(20);

        return view('admin.pages.index', compact('events', 'media', 'posts', 'talents'));
    }

    //login
    public function login(Request $request){

        if($this->admin){
            return redirect()->route('admin-dashboard');    
        }

        if(request()->method() == 'GET'){   
            return view('admin.pages.auth.login');
        }

        $rules = array(
            'email' => 'required',
            'password' => 'required'
        );
        
        if(!$this->validation($request, $rules)){
            return back()->withInput();
        }

        if (Auth::guard('admins')->attempt($request->except(['_token']))){
            
            return redirect()->route('admin-dashboard');            
        }
        else{

            Session::flash('error', 'Invalid details, please ensure you have entered in the correct details and that you have the authority to access this site.');

            return back()->withInput();
        }
    }

    //logout
    public function logout(){
        
        Auth::guard('admins')->logout();

        return redirect()->route('index');
    }

    //send password reset email
    public function forgotPassword(Request $request){

        if(request()->method() == 'GET'){   

            return view('admin.pages.auth.forgotPassword');
        }

        $rules = array(
            'email' => 'required'
        );
        
        if(!$this->validation($request, $rules)){
            return back()->withInput();
        }

        $admin = Admin::where('email', $request->email)->first();

        if(!$admin){
            
            Session::flash('error', 'We can\'t find a user with that e-mail address.');

            return back()->withInput();
        }

        $token = $this->broker()->createToken($admin);

        try {
            $data = array(
                'token' => $token,
                'name' => $admin->name
            );

            $this->emailSender($request->email, $data, 'Password Reset', 'emails.passwordReset');
        }

        catch(Exception $e){

            Session::flash('error', 'Please check your internet connection and try again');

            return back();
        }

        Session::flash('success', 'Password reset email sent successfully');    
        
        return back();
    }

    //show password reset form
    public function passwordReset($token){
       
        return view('admin.pages.auth.resetPassword', compact('token'));
    }

    public function uploadImage($request, $requestName, $filenameStart, $fileLocation, $image = null){
            
        if(!$image){
            $image = $request->file($requestName);              
        }
        
        $filename = $filenameStart . '-' . time() . '.' . $image->getClientOriginalExtension();              

        $image->storeAs($fileLocation, $filename);

        return $filename;
    }

    public function removeImage($data, $requestName, $fileLocation, $image = null){

        if($data[$requestName]){

            $filename = $data[$requestName];    

            $location = public_path($fileLocation . $filename);

            if(file_exists($location)){

                unlink($location);
            }
        }

        if($image){

            $location = public_path($fileLocation . $image);

            if(file_exists($location)){

                unlink($location);
            }
        }

        return null;
    }

    public function getOrderNumber($request, $data, $slug = null){

        $existingOrderNumber = $data->where('order', $request->order)->first();

        $nextOrderNumber = 1;

        if($existingOrderNumber){
            
            $previousOrderNumber = $data->where('slug', $slug)->first();

            if($previousOrderNumber){
                
                $existingOrderNumber->order = $previousOrderNumber->order;
                
                $newOrderNumber = 1;

                while($data->where('order', $newOrderNumber)->exists()){
                    $newOrderNumber++;
                }

                $previousOrderNumber->order = $newOrderNumber;

                $previousOrderNumber->save();
            }
            
            else{
                
                $newOrderNumber = 1;

                while($data->where('order', $newOrderNumber)->exists()){
                    $newOrderNumber++;
                }

                $existingOrderNumber->order = $newOrderNumber;
            }

            $existingOrderNumber->save();
        }
        
        while($data->where('order', $nextOrderNumber)->exists()){
            $nextOrderNumber++;
        }

        return $nextOrderNumber;
    }

    //uploading images from summer api
    public function summerUploader(Request $request){
    
        if($request->hasFile('image')){
            
            $image = $request->file('image');               
            
            $filename = $this->generateRandomString(10) . '.' . strtolower($image->getClientOriginalExtension());
            
            $image->storeAs('img/uploads/', $filename);      
            
            return response()->json(['status' => true, 'location' => asset('img/uploads/' . $filename)]);
        }          
    }

    //random string generator
    public function generateRandomString($length) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }

    //date time string parser
    public function parseDateTimeString($value){

        return Carbon::parse($value)->toDateTimeString();
    }
}
