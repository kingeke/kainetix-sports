<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\{Auth, Validator, Input, Redirect};
use Illuminate\Http\Request;
use Session, Password;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    public function __construct()
    {
        $this->middleware('guest');
        
    }

    protected function guard(){
        return Auth::guard('admins');
    }

    protected function broker(){
        return Password::broker('admins');
    }

    public function reset(Request $request){

        $rules = array(
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6'
        );

        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()){
           Session::flash('error',  $validator->messages()->first());

           return back()->withInput();
        }

        $this->broker()->validator(function ($credentials) {
            return mb_strlen($credentials['password']) >= 6;
        });

        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );

        if ($response == Password::PASSWORD_RESET) {
            
            Session::flash('success', 'Password reset successful');

            return redirect()->route('admin-dashboard');
        } else {

            Session::flash('error', 'This password reset token is invalid');

            return back()->withInput();
        }
    }
}
