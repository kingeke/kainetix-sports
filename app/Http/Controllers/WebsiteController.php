<?php

namespace App\Http\Controllers;

use App\{About, Expertise, Event, BookedEvent, Gallery, Post, Comment, Video, Talent};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use Mail;
use App\Http\Traits\{CustomTraits};
use Carbon\Carbon;
use Faker\Factory as Faker;


class WebsiteController extends Controller
{
    use CustomTraits; 

    public function index(){
        
        $events = Event::latest()->take(3)->get();

        $gallery = Gallery::latest()->take(10)->get();

        $videos = Video::latest()->take(6)->get();

        $posts = Post::where('visible', true)->latest()->take(2)->get();

        return view('master.pages.index', compact('events', 'gallery', 'posts', 'videos'));
    }

    public function about(){

        $aboutContent = About::orderBy('order')->get();

        return view('master.pages.about', compact('aboutContent'));
    }

    public function expertise(){

        $expertiseContent = Expertise::orderBy('order')->get();

        return view('master.pages.expertise', compact('expertiseContent'));
    }

    public function videos(){

        $videos = Video::latest()->paginate(6);

        return view('master.pages.media.videos', compact('videos'));
    }

    public function events(Request $request){

        if(request()->method() == 'GET'){

            $events = Event::latest()->paginate(6);

            $date = Carbon::now()->toDateTimeString();

            return view('master.pages.media.events', compact('events', 'date'));
        }

        $rules = array(
            'name' => 'Required|Min:3',
            'email' => 'Required|Email',
            'number' => 'Required|Min:3',
            'g-recaptcha-response' => 'Required'
        );

        if(!$this->validation($request, $rules)){
            return back()->withInput();
        }

        $recaptcha = $request['g-recaptcha-response'];

        if($this->recaptchaValid($recaptcha)){    

            $event = Event::where('slug', $request->slug)->first();        

            $participant = BookedEvent::firstOrNew([
                'event_id' => $event->id,
                'email' => $request->email,
                'name' => $request->name,
                'number' => $request->number
            ]);

            if($participant->exists){

                Session::flash('error', 'You have already booked this event');

                return back()->withInput();
            }
            else{

                $data = array(
                    'event' => $event->title,
                    'name' => $request->name,
                    'email' => $request->email,
                    'number' => $request->number
                );
    
                if($this->emailSender('info@kainetixsports.com', $data, 'New Event Participant', 'emails.bookedEvent')){        

                    Session::flash('success', 'Event booked, thank you.');

                    $participant->save();

                    return back();

                }

                else{
                    return back()->withInput();
                }
            }

        }
        else{                
            return back()->withInput();
        }        
    }

    public function posts(){

        $posts = Post::where('visible', true)->latest()->with('comments')->paginate(5);

        return view('master.pages.media.posts', compact('posts'));
    }

    public function post(Request $request, $slug){
        
        $post = Post::where('slug', $slug)->where('visible', true)->first();

        if(!$post){

            Session::flash('error', 'Post not found');

            return redirect()->route('index');
        }

        $comments = $post->comments()->get();

        if(request()->method() == 'GET'){

            return view('master.pages.media.post', compact('post', 'comments'));
        }

        $rules = array(
            'name' => 'Required|Min:3',
            'email' => 'Required|Email',
            'comment' => 'Required|Min:3',
            'g-recaptcha-response' => 'Required'
        );

        if(!$this->validation($request, $rules)){
            return back()->withInput();
        }

        $recaptcha = $request['g-recaptcha-response'];

        if($this->recaptchaValid($recaptcha)){    

            $data = array(
                'post' => $post->title,
                'name' => $request->name,
                'email' => $request->email,
                'comment' => $request->comment
            );

            if($this->emailSender('info@kainetixsports.com', $data, 'New Comment', 'emails.newComment')){  
                
                if($request->child_id){

                    $user = Comment::where('id', $request->child_id)->first();

                    $data = array(
                        'title' => $post->title,
                        'name' => $user->name,
                        'slug' => $post->slug
                    );                    

                    $this->emailSender($user->email, $data, 'New Reply To Comment', 'emails.newReply');
                    
                }
                
                Comment::create([
                    'post_id' => $post->id,
                    'parent_id' => $request->comment_id ?? null,
                    'child_id' => $request->child_id ?? null,
                    'name' => $request->name,
                    'email' => $request->email,
                    'comment' => $request->comment
                ]);
        
                Session::flash('success', 'Comment received, thank you.');
        
                return back();

            }

            else{
                return back()->withInput();
            }

        }
        else{                
            return back()->withInput();
        }  
    }

    public function gallery(){

        $gallery = Gallery::latest()->paginate(25);

        return view('master.pages.media.gallery', compact('gallery'));
    }

    public function talents(){
        
        $talents = Talent::latest()->paginate(6);

        return view('master.pages.talents', compact('talents'));
    }

    public function contact(Request $request){
        
        if(request()->method() == 'GET'){
            return view('master.pages.contact');
        }

        $name = $request['name'];
        $email = $request['email'];
        $subject = $request['subject'];
        $message = $request['message'];
        $recaptcha = $request['g-recaptcha-response'];

        $rules = array(
            'name' => 'Required|Min:4',
            'email' => 'Required|Email',
            'subject' => 'Required|Min:4',
            'message' => 'Required|Min:15',
            'g-recaptcha-response' => 'Required'
        );


        if(!$this->validation($request, $rules)){
            return Redirect::to('/contact#contact')->withInput();
        }
        
        else{
            
            if ($this->recaptchaValid($recaptcha)){       

                $data = array(
                    'name' => $name,
                    'email' => $email,
                    'subject' => $subject,
                    'contact_message' => $message
                );
                
                if($this->emailSender('info@kainetixsports.com', $data, 'New Contact Message', 'emails.contact')){

                    Session::flash('success', 'Email sent successfully, we would get back to you shortly. Thank you');
                    
                    return back();

                }

                else{
                    
                    return back()->withInput();
                }                
            }

            else{
                
                return Redirect::to('/contact#contact')->withInput();
            }
        }   
    }
}
