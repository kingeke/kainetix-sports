<?php

namespace App\Http\Traits;

use App\{Admin};
use Illuminate\Http\Request;
use Session, Hash;

trait AdminTraits {

    public function createAdmin(Request $request) {

        if(request()->method() == 'GET'){

            return view('admin.pages.admins.create');
        }

        $rules = array(
            'name' => 'required',
            'email' => 'required|email|Unique:admins,email',
            'password' => 'Required|Alpha_Num|Min:6|Confirmed'
        );

        if(!$this->validation($request, $rules)){
            return back()->withInput();
        }

        $request['password'] = Hash::make($request->password);

        Admin::create($request->all());

        Session::flash('success', 'Admin created successfully');

        return back();
    }

    public function viewAdmins(){

        $admins = Admin::latest()->with('posts')->get();

        return view('admin.pages.admins.index', compact('admins'));
    }

    public function editAdmin(Request $request, $slug, $type){

        $admin = Admin::where('slug', $slug)->first();

        if(request()->method() == 'GET'){   

            return view('admin.pages.admins.edit', compact('type'))->with('theAdmin', $admin);            
        }

        if($type == 'info'){

            $rules = array(
                'name' => 'required',
                'email' => 'required|email|Unique:admins,email,' . $admin->id
            );
    
            if(!$this->validation($request, $rules)){
                return back()->withInput();
            }
        }

        if($type == 'password'){
            
            $rules = array(
                'password' => 'Required|Alpha_Num|Min:6|Confirmed'
            );
    
            if(!$this->validation($request, $rules)){
                return back()->withInput();
            }

            $request['password'] = Hash::make($request->password);
        }        

        $admin->update($request->all());

        Session::flash('success', 'Updated successfully');

        return redirect()->route('edit-admin', [$admin->slug, $type]);

    }

    public function deleteAdmin($slug){

        Admin::where('slug', $slug)->first()->delete();

        Session::flash('success','Deleted successfully');

        return back();
    }
}