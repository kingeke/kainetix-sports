<?php

namespace App\Http\Traits;
use Illuminate\Support\Facades\{Validator};
use Session;
use Mail;
use GuzzleHttp\Client;

trait CustomTraits {

    //sending emails
    public function emailSender($to, $data, $subject, $view){
        try{

            Mail::send($view, $data, function($message) use ($to, $subject) {
                $message->to($to);
                $message->subject($subject);
                $message->from('info@kainetixsports.com', 'Kainetix Sports');
            });

            return true;
        }

        catch(Exception $e){

            Session::flash('error', 'Please check your internet connection and try again');

            return false;
        }
       
    }

    public function validation($request, $rules){

        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()){

            Session::flash('error', $validator->messages()->first());

            return false;
        }

        return true;
    }

    public function recaptchaValid($recaptcha){

        if($recaptcha){

            try{
                
                $client = new Client();

                $response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
                    'form_params' => array(
                        'secret' => '6LfbOakUAAAAAFKyHNyzOtD3JPsI32-Js9K-9F1R',
                        'response' => $recaptcha
                    )
                ]);
                                
                $result = json_decode($response->getBody()->getContents()); 

                if($result->success){
                    return true;
                }
                else{
    
                    Session::flash('error', 'Recaptcha expired, please refresh the page and try again');
    
                    return false;
                }
            }

            catch(Exception $e){

                Session::flash('error', 'Please check your internet connection and try again');
    
                return false;
            }
          
        }
        else{

            Session::flash('error', 'Please wait for google recaptcha to verify if you\'re a human, if it does not please contact us');

            return false;
        }
    }
}