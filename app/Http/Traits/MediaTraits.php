<?php

namespace App\Http\Traits;

//website traits
use App\Http\Traits\mediaTraits\{EventTraits, GalleryTraits, PostsTraits, VideoTraits};

trait MediaTraits {

    use EventTraits, GalleryTraits, PostsTraits, VideoTraits;
}