<?php

namespace App\Http\Traits;

use App\{Talent};
use Illuminate\Http\Request;
use Session, Hash;
use Carbon\Carbon;
use Illuminate\Support\Str;

trait TalentTraits {

    public function talentRules($talent = null){

        $rules = array(
            'name' => 'required|Min:3',
            'email' => 'nullable|email|Unique:talents,email' . ($talent ? ',' . $talent->id : ''),
            'phone' => 'nullable|numeric',
            'sport' => 'nullable|Min:3',
            'dob' => 'nullable|date',
            'socials.*' => 'nullable|url',
            'about' => 'required|Min:3',
            'images.*' => 'nullable|Image|max:2000',
            'avatar' => 'nullable|Image|max:2000'
        );

        return $rules;
    }

    public function createTalent(Request $request) {

        if(request()->method() == 'GET'){

            return view('admin.pages.talents.create');
        }

        if(!$this->validation($request, $this->talentRules())){
            return back()->withInput();
        }

        $images = null;

        $avatar = null;

       
        if($request->images){

            $images = [];

            foreach($request->images as $key => $image){

                $filenameStart = Str::slug($request->name, '-') . '-' . $key;
        
                $filename = $this->uploadImage(null, null, $filenameStart, 'img/talents/', $image);
                
                array_push($images, $filename);
            }
        }

        if($request->avatar){

            $filenameStart = Str::slug($request->name, '-');

            $avatar = $this->uploadImage($request, 'avatar', $filenameStart, 'img/talents/');
        }

        Talent::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'sport' => $request->sport,
            'dob' => $request->dob ? Carbon::parse($request->dob)->toDateString() : null,
            'socials' => !array_filter($request->socials) ? null: array_filter($request->socials),
            'about' => $request->about,
            'avatar' => $avatar,
            'images' => $images
        ]);

        Session::flash('success', 'Created successfully');

        return back();
    }

    public function viewTalents(){

        $talents = Talent::latest()->paginate(100);

        return view('admin.pages.talents.index', compact('talents'));
    }

    public function viewTalent($slug){

        $talent = Talent::where('slug', $slug)->first();

        return view('admin.pages.talents.view', compact('talent'));
    }

    public function editTalent(Request $request, $slug){

        $talent = Talent::where('slug', $slug)->first();

        if(request()->method() == 'GET'){   

            return view('admin.pages.talents.edit', compact('talent'));       
        }

        if(!$this->validation($request, $this->talentRules($talent))){
            return back()->withInput();
        }

        $images = $talent->images ?? [];

        if($request->removeImages){
            
            foreach($images as $image){
                $this->removeImage(null, null, 'img/talents/', $image);
            }

            $images = null;
        }

        if($request->images){

            foreach($request->images as $image){

                $key = count((array) $images);

                $filenameStart = Str::slug($request->name, '-') . '-' . $key;
        
                $filename = $this->uploadImage(null, null, $filenameStart, 'img/talents/', $image);
                
                array_push($images, $filename);
            }
        }

        $avatar = $talent->avatar;

        if($request->removeAvatar){
            $avatar = $this->removeImage($talent, 'avatar', 'img/talents/');
        }

        if($request->avatar){

            $filenameStart = Str::slug($request->name, '-');

            $avatar = $this->removeImage($talent, 'avatar', 'img/talents/');

            $avatar = $this->uploadImage($request, 'avatar', $filenameStart, 'img/talents/');
        }

        $talent->update([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'sport' => $request->sport,
            'dob' => $request->dob ? Carbon::parse($request->dob)->toDateString() : null,
            'socials' => !array_filter($request->socials) ? null: array_filter($request->socials),
            'about' => $request->about,
            'avatar' => $avatar,
            'images' => $images
        ]);

        Session::flash('success', 'Updated successfully');

        return redirect()->route('edit-talent', $talent->slug);

    }

    public function deleteTalent($slug){

        $talent = Talent::where('slug', $slug)->first();

        if(count((array) $talent->images) > 0){
            foreach($talent->images as $image){
                $this->removeImage(null, null, 'img/talents/', $image);
            }
        }

        if($talent->avatar){
            $this->removeImage(null, null, 'img/talents/', $talent->avatar);
        }

        $talent->delete();

        Session::flash('success','Deleted successfully');

        return redirect()->route('view-talents');
    }

    public function deleteImage(Request $request, $slug){
        
        $talent = Talent::where('slug', $slug)->first();

        $images = $talent->images;

        if(count((array) $images) > 0){

            $image = array($request->key);

            $images = array_diff($images, $image);

            $images = implode(',', $images);

            $images = explode(',', $images);

            $this->removeImage(null, null, 'img/talents/', $request->key);

            $talent->update(['images' => $images[0] == "" ? null : $images]);

            return response()->json(null, 200);
        }
    }
}