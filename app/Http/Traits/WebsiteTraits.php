<?php

namespace App\Http\Traits;

//website traits
use App\Http\Traits\websiteTraits\{AboutTraits, ExpertiseTraits};

trait WebsiteTraits {

    use AboutTraits, ExpertiseTraits;
}