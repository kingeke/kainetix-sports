<?php

namespace App\Http\Traits\mediaTraits;

use App\{Event};
use Illuminate\Http\Request;
use Session;

trait EventTraits {

    public function eventRules(){
        
        $rules = array(
            'title' => 'required|Min:3',
            'image' => 'nullable|Image|Max:2000',
            'time' => 'required',
            'venue' => 'required|Min:3',
            'description' => 'required'
        );

        return $rules;
    }

    public function viewEvents(){

        $events = Event::with('participants')->latest()->paginate(100);

        return view('admin.pages.media.events.index', compact('events'));
    }

    public function viewEvent($slug){

        $event = Event::where('slug', $slug)->first();

        $participants = $event->participants()->latest()->paginate(100, ['*'], 'participants');   

        return view('admin.pages.media.events.view', compact('event', 'participants'));
    }

    public function createEvent(Request $request){

        if(request()->method() == 'GET'){

            return view('admin.pages.media.events.create');
        }

        if(!$this->validation($request, $this->eventRules())){
            return back()->withInput();
        }

        $image = null;

        if($request->image){
            $image = $this->uploadImage($request, 'image', $request->title, 'img/events/');
        }

        Event::create([
            'title' => $request->title,
            'time' => $this->parseDateTimeString($request->time),
            'venue' => $request->venue,
            'description' => $request->description,
            'bookings' => $request->bookings ? true : false,
            'image' => $image
        ]);

        Session::flash('success', 'Created successfully');

        return back();
        
    }

    public function editEvent(Request $request, $slug){
        
        $event = Event::where('slug', $slug)->first();

        if(request()->method() == 'GET'){   

            return view('admin.pages.media.events.edit', compact('event'));
        }

        if(!$this->validation($request, $this->eventRules())){
            return back()->withInput();
        }

        $image = $event->image;

        if($request->removeImage){
            $image = $this->removeImage($event, 'image', 'img/events/');
        }

        if($request->image){
            
            $this->removeImage($event, 'image', 'img/events/');

            $image = $this->uploadImage($request, 'image', $request->title, 'img/events/');
        }

        $event->update([
            'title' => $request->title,
            'time' => $this->parseDateTimeString($request->time),
            'venue' => $request->venue,
            'description' => $request->description,
            'bookings' => $request->bookings ? true : false,
            'image' => $image
        ]);

        Session::flash('success', 'Updated successfully');

        return redirect()->route('media-event-edit', $event->slug);
    }

    public function deleteEvent($slug){
        
        $event = Event::where('slug', $slug)->first();

        $this->removeImage($event, 'image', 'img/events/');

        $event->delete();

        Session::flash('success', 'Deleted successfully');

        return redirect()->route('media-events-view');
    }
}