<?php

namespace App\Http\Traits\mediaTraits;

use App\{Gallery};
use Illuminate\Http\Request;
use Session;

trait GalleryTraits {

    public function galleryRules(){
       
        $rules = array(
            'title' => 'Nullable|Min:3',
            'description' => 'nullable|Min:3',
            'image' => 'required|Image|max:2000'
        );

        return $rules;
    }

    public function viewGallery(){

        $gallery = Gallery::latest()->paginate(100);

        return view('admin.pages.media.gallery.index', compact('gallery'));
    }

    public function createGallery(Request $request){

        if(request()->method() == 'GET'){
            return view('admin.pages.media.gallery.create');
        }

        if(!$this->validation($request, $this->galleryRules())){
            return back()->withInput();
        }      

        if($request->image){
            $image = $this->uploadImage($request, 'image', $request->title ?? 'Kainetix-Image', 'img/gallery/');
        }

        Gallery::create([
            'title' => $request->title ?? 'Kainetix Image',
            'description' => $request->description,
            'image' => $image
        ]);

        Session::flash('success', 'Created successfully');

        return back();
        
    }

    public function editGallery(Request $request, $slug){

        $gallery = Gallery::where('slug', $slug)->first();

        if(request()->method() == 'GET'){   

            return view('admin.pages.media.gallery.edit', compact('gallery'));
        }

        $rules = array(
            'title' => 'Nullable|Min:3',
            'description' => 'nullable|Min:3',
            'image' => 'nullable|Image|max:2000'
        );

        if(!$this->validation($request, $rules)){
            return back()->withInput();
        }

        $image = $gallery->image;

        if($request->image){

            $this->removeImage($gallery, 'image', 'img/gallery/');

            $image = $this->uploadImage($request, 'image', $request->title ?? 'Kainetix-Image', 'img/gallery/');
        }

        $gallery->update([
            'title' => $request->title ?? 'Kainetix Image',
            'description' => $request->description,
            'image' => $image
        ]);

        Session::flash('success', 'Updated successfully');

        return redirect()->route('media-gallery-edit', $gallery->slug);
    }

    public function deleteGallery($slug){
        
        $gallery = Gallery::where('slug', $slug)->first();

        $this->removeImage($gallery, 'image', 'img/gallery/');

        $gallery->delete();

        Session::flash('success', 'Deleted successfully');

        return back();
    }
}