<?php

namespace App\Http\Traits\mediaTraits;

use App\{Post, Comment};
use Illuminate\Http\Request;
use Session;

trait PostsTraits {

    public function postRules(){
        
        $rules = array(
            'title' => 'required|Min:3',
            'description' => 'required',
            'visible' => 'required',
            'image' => 'nullable|Image|Max:2000'
        );

        return $rules;
    }

    public function viewPosts(){

        $posts = Post::with('comments')->latest()->paginate(100);

        return view('admin.pages.media.posts.index', compact('posts'));
    }

    public function viewPost($slug){

        $post = Post::where('slug', $slug)->first();

        $comments = $post->comments()->latest()->paginate(100, ['*'], 'comments');   

        return view('admin.pages.media.posts.view', compact('post', 'comments'));
    }

    public function createPost(Request $request){

        if(request()->method() == 'GET'){

            return view('admin.pages.media.posts.create');
        }

        if(!$this->validation($request, $this->postRules())){
            return back()->withInput();
        }

        $image = null;

        if($request->image){
            $image = $this->uploadImage($request, 'image', $request->title, 'img/news/');
        }

        Post::create([
            'title' => $request->title,
            'description' => $request->description,
            'visible' => $request->visible == 'Yes' ? true : false,
            'image' => $image,
            'admin_id' => $this->admin->id
        ]);

        Session::flash('success', 'Created successfully');

        return back();
        
    }

    public function editPost(Request $request, $slug){
        
        $post = Post::where('slug', $slug)->first();

        if(request()->method() == 'GET'){   

            return view('admin.pages.media.posts.edit', compact('post'));
        }

        if(!$this->validation($request, $this->postRules())){
            return back()->withInput();
        }

        $image = $post->image;

        if($request->removeImage){
            $image = $this->removeImage($post, 'image', 'img/news/');
        }

        if($request->image){
            
            $this->removeImage($post, 'image', 'img/news/');

            $image = $this->uploadImage($request, 'image', $request->title, 'img/news/');
        }

        $post->update([
            'title' => $request->title,
            'description' => $request->description,
            'visible' => $request->visible == 'Yes' ? true : false,
            'image' => $image
        ]);

        Session::flash('success', 'Updated successfully');

        return redirect()->route('media-post-edit', $post->slug);
    }

    public function deletePost($slug){
        
        $post = Post::where('slug', $slug)->first();

        $this->removeImage($post, 'image', 'img/news/');

        $post->delete();

        Session::flash('success', 'Deleted successfully');

        return redirect()->route('media-posts-view');
    }

    public function deleteComment($id){
        
        $comment = Comment::where('id', $id)->first();
        
        $comment->delete();

        Session::flash('success', 'Deleted successfully');

        return back();
    }
}