<?php

namespace App\Http\Traits\mediaTraits;

use App\{Video};
use Illuminate\Http\Request;
use Session;

trait VideoTraits {

    public function videoRules(){
        
        $rules = array(
            'title' => 'required|Min:3',
            'description' => 'nullable|min:3',
            'youtube_id' => 'required'
        );

        return $rules;
    }

    public function viewVideos(){

        $videos = Video::latest()->paginate(100);

        return view('admin.pages.media.videos.index', compact('videos'));
    }

    public function createVideo(Request $request){

        if(request()->method() == 'GET'){

            return view('admin.pages.media.videos.create');
        }

        if(!$this->validation($request, $this->videoRules())){
            return back()->withInput();
        }

        Video::create($request->all());

        Session::flash('success', 'Created successfully');

        return back();
        
    }

    public function editVideo(Request $request, $slug){
        
        $video = Video::where('slug', $slug)->first();

        if(request()->method() == 'GET'){   

            return view('admin.pages.media.videos.edit', compact('video'));
        }

        if(!$this->validation($request, $this->videoRules())){
            return back()->withInput();
        }

        $video->update($request->all());

        Session::flash('success', 'Updated successfully');

        return redirect()->route('media-video-edit', $video->slug);
    }

    public function deleteVideo($slug){
        
        $video = Video::where('slug', $slug)->delete();

        Session::flash('success', 'Deleted successfully');

        return redirect()->route('media-videos-view');
    }
}