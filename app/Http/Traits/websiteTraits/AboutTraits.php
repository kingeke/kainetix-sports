<?php

namespace App\Http\Traits\websiteTraits;

use App\{About};
use Illuminate\Http\Request;
use Session;

trait AboutTraits {

    public function aboutRules(){

        $rules = array(
            'title' => 'required',
            'description' => 'required',
            'order' => 'required'
        );

        return $rules;
    }

    public function viewAbouts(){

        $aboutContent = About::latest()->get();

        return view('admin.pages.website.about.index', compact('aboutContent'));
    }

    public function createAbout(Request $request){

        if(request()->method() == 'GET'){

            $aboutLength = About::count();

            return view('admin.pages.website.about.create', compact('aboutLength'));
        }

        if(!$this->validation($request, $this->aboutRules())){
            return back()->withInput();
        }

        About::create([
            'order' => $this->getOrderNumber($request, new About),
            'title' => $request->title,
            'description' => $request->description
        ]);

        Session::flash('success', 'Created successfully');

        return back();
        
    }

    public function editAbout(Request $request, $slug){
        
        $about = About::where('slug', $slug)->first();

        if(request()->method() == 'GET'){
            
            $aboutLength = About::count();

            return view('admin.pages.website.about.edit', compact('about', 'aboutLength'));
        }

        if(!$this->validation($request, $this->aboutRules())){
            return back()->withInput();
        }

        $order = $about->order;

        if($request->order != $order){
            $order = $this->getOrderNumber($request, new About, $slug);
        }

        $about->update([
            'order' => $order,
            'title' => $request->title,
            'description' => $request->description
        ]);

        Session::flash('success', 'Updated successfully');

        return redirect()->route('website-about-edit', $about->slug);
    }

    public function deleteAbout($slug){
        
        About::where('slug', $slug)->delete();

        Session::flash('success', 'Deleted successfully');

        return back();
    }
}