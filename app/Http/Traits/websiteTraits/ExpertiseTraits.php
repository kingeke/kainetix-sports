<?php

namespace App\Http\Traits\websiteTraits;

use App\{Expertise};
use Illuminate\Http\Request;
use Session;

trait ExpertiseTraits {

    public function expertiseRules(){
       
        $rules = array(
            'title' => 'required',
            'description' => 'required',
            'image' => 'nullable|Image|max:2000',
            'order' => 'required'
        );

        return $rules;
    }

    public function viewExpertises(){

        $expertiseContent = Expertise::latest()->get();

        return view('admin.pages.website.expertise.index', compact('expertiseContent'));
    }

    public function createExpertise(Request $request){

        if(request()->method() == 'GET'){

            $expertiseLength = Expertise::count();

            return view('admin.pages.website.expertise.create', compact('expertiseLength'));
        }

        if(!$this->validation($request, $this->expertiseRules())){
            return back()->withInput();
        }      

        $image = null;

        if($request->image){
            $image = $this->uploadImage($request, 'image', $request->title, 'img/website/expertise/');
        }

        Expertise::create([
            'order' => $this->getOrderNumber($request, new Expertise),
            'title' => $request->title,
            'description' => $request->description,
            'image' => $image
        ]);

        Session::flash('success', 'Created successfully');

        return back();
        
    }

    public function editExpertise(Request $request, $slug){

        $expertise = Expertise::where('slug', $slug)->first();

        if(request()->method() == 'GET'){   

            $expertiseLength = Expertise::count();

            return view('admin.pages.website.expertise.edit', compact('expertise', 'expertiseLength'));
        }

        if(!$this->validation($request, $this->expertiseRules())){
            return back()->withInput();
        }

        $image = $expertise->image;

        if($request->removeImage){
            $image = $this->removeImage($expertise, 'image', 'img/website/expertise/');
        }

        if($request->image){

            $this->removeImage($expertise, 'image', 'img/website/expertise/');

            $image = $this->uploadImage($request, 'image', $request->title, 'img/website/expertise/');
        }

        $order = $expertise->order;

        if($request->order != $order){
            $order = $this->getOrderNumber($request, new Expertise, $slug);
        }

        $expertise->update([
            'order' => $order,
            'title' => $request->title,
            'description' => $request->description,
            'image' =>  $image
        ]);

        Session::flash('success', 'Updated successfully');

        return redirect()->route('website-expertise-edit', $expertise->slug);
    }

    public function deleteExpertise($slug){
        
        $expertise = Expertise::where('slug', $slug)->first();

        $this->removeImage($expertise, 'image', 'img/website/expertise/');

        $expertise->delete();

        Session::flash('success', 'Deleted successfully');

        return back();
    }
}