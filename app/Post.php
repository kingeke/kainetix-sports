<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Post extends Model
{
    use HasSlug;
    
    protected $fillable = ['title', 'description', 'image', 'visible', 'admin_id'];
    
    public function getSlugOptions() : SlugOptions{
        return SlugOptions::create()->generateSlugsFrom('title')->saveSlugsTo('slug');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function admin(){
        return $this->belongsTo('App\Admin');
    }

    public function delete(){
        
        // delete all related photos 
        $this->comments()->delete();

        //delete the event
        return parent::delete();
    }
}
