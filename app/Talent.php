<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Talent extends Model
{
    protected $table = "talents";
    
    use HasSlug;
    
    protected $fillable = ['name', 'email', 'phone', 'about', 'avatar', 'images', 'sport', 'dob', 'socials'];

    protected $dates = ['dob'];
    
    public function getSlugOptions() : SlugOptions{
        return SlugOptions::create()->generateSlugsFrom('name')->saveSlugsTo('slug');
    }

    protected $casts = [
        'socials' => 'array',
        'images' => 'array'
    ];
}
