<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

use Faker\Factory as Faker;

class All extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $abouts  = array([
                'slug' => 'kainetix-sports',
                'title' => 'KAINETIX SPORTS',
                'description' => "<p><span style='font-family: Arial;'>﻿</span><span style='font-family: Arial;'>﻿</span><span style='font-family: Arial;'>﻿</span><span style='font-family: Arial;'>﻿</span><span style='font-family: Arial;'>﻿</span><span style='font-family: Arial;'>Kainetix Sports Management is a talent management and sports marketing specialist, taking advantage of the abundance of talents in the African region through the following:</span></p><p segoe='' ui',='' roboto,='' 'helvetica='' neue',='' arial,='' 'noto='' sans',='' sans-serif,='' 'apple='' color='' emoji',='' 'segoe='' ui='' symbol',='' emoji';='' font-size:='' 16px;'='' style='margin-bottom: 1rem; color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, '></p><ul segoe='' ui',='' roboto,='' 'helvetica='' neue',='' arial,='' 'noto='' sans',='' sans-serif,='' 'apple='' color='' emoji',='' 'segoe='' ui='' symbol',='' emoji';='' font-size:='' 16px;'='' style='color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, '><li><span style='font-family: Arial;'>Provision of sporting consultancy which includes personality branding and management on a global stage for established and budding star athletes.</span></li><li><span style='font-family: Arial;'>Acquisition of image rights and intellectual property and the global management and marketing of these rights using the most effective communication channels.</span></li><li><span style='font-family: Arial;'>Event marketing and celebrity management with emphasis on media partnerships, sponsorship and brand extension management.</span></li><li><span style='font-family: Arial;'>Discovery and management of youth talent for the global and local market.</span></li></ul>",     
                'order' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'slug' => 'management-team',
                'title' => 'MANAGEMENT TEAM',
                'description' => "<p><span style='font-family: Arial;'>﻿</span><font color='#212529' face='-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica Neue, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji'><span style='font-size: 24px; font-family: Arial;'>Otisi Ukiwe:&nbsp;</span></font><span style='font-size: 12px; font-family: Arial;'>﻿</span><span style='font-size: 14px;'><span style='font-family: Arial;'>﻿</span><span style='font-size: 16px; font-family: Arial;'>The principal representative for Kainetix Sports Management is Otisi Ukiwe. Otisi is a graduate of&nbsp;</span></span><span style='font-size: 16px; font-family: Arial;'>University of Kent, UK. Otisi studied business administration and management science.&nbsp;</span></p><p><span style='font-size: 16px; font-family: Arial;'>Otisi worked in the Corporate Venturing and Business Incubation unit of Nextzon Business Services in Lagos, Nigeria. Mr. Ukiwe has also worked with International Sports Management Africa, where he was responsible for planning and executing several events for charity organizations and the discovery of elite soccer/football talent.</span><br></p><p><font color='#212529' face='Arial'><span style='font-size: 24px; font-family: Arial;'>Achike Ofodile</span></font><font color='#212529' face='-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica Neue, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji'><span style='font-size: 24px; font-family: Arial;'>:&nbsp;</span></font><span style='font-size: 12px; font-family: Arial;'>﻿</span><span style='font-size: 14px; font-family: Arial;'>﻿</span><span style='font-size: 16px; font-family: Arial;'>Achike is a registered Intermediary with the Nigerian Football Federation. A law graduate from the University of Kent with a masters in Sports Law from Nottingham Law School, he has worked in the Sports development sector since 2006. As Vice President (Sport) of Kent Union, he provided leadership to Kent Union staff and representatives to ensure sports and other recreational activities were effectively and fairly run providing opportunities for all students as well as providing support to over 50 sport clubs, working closely with staff and students.&nbsp;</span><br></p><p><span style='font-size: 16px; font-family: Arial;'>Achike’s experience has seen him work with Canterbury City Council and Kent County Council in sports development and social empowerment using sports, as well as talent management and development across Nigeria.</span><br></p><p></p>", 
                'order' => 2,    
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        );

        $expertises = array(
            [
                'slug' => 'talent-management',
                'title' => 'Talent Management',
                'description' => "<p style='margin-bottom: 1rem; color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px;'><span style='font-family: Arial;'>This involves the discovery and management of careers of exceptional sports talent across the country through a network of scouts, coaches, talent hunt events and agents. The sports include football, basketball and athletics.</span></p><p style='margin-bottom: 1rem; color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px;'><span style='font-family: Arial;'>Talent Management will include the sourcing of professional clubs for the sportsman or sportswoman, sourcing of scholarship opportunities and negotiation of contracts in the best interest of the sports person.</span><span style='font-family: Arial;'>﻿</span></p>",    
                'image' => 'Talent Management-1561665571.jpg',
                'order' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'slug' => 'sports-management',
                'title' => 'Sports Management',
                'description' => "<p><span style='font-family: Arial;'>﻿</span><font face='Arial'>Development of new sports entertainment television concepts and events and the acquisition of the rights for established television shows and events.</font><br></p>",    
                'image' => 'Sports Management-1561602491.jpg',
                'order' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'slug' => 'sports-consultancy',
                'title' => 'Sports Consultancy',
                'description' => "<p><span style='font-family: Arial;'>﻿</span><span style='font-family: Arial;'>﻿</span><span style='font-family: Arial;'>﻿</span><font face='Arial'>Provision of advice to businesses and government bodies who are interested in getting involved in one area of business development or the other.</font><br></p>",
                'image' => 'Sports Consultancy-1561602597.jpg',
                'order' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        );

        $admins = array([
            'name' => 'Chinonso Eke',
            'slug' => 'chinonso-eke',
            'email' => 'chinonsoeke@gmail.com',
            'password' => '$2y$10$qmOC7anO9c/gkwMjKFDsM.0P/K9oihtcMwkHM6l/FXSIMIlbcSxFi',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ], [
                'name' => 'Otisi Ukiwe',
                'slug' => 'otisi-ukiwe',
                'email' => 'otisi@kainetixsports.com',
                'password' => '$2y$10$qmOC7anO9c/gkwMjKFDsM.0P/K9oihtcMwkHM6l/FXSIMIlbcSxFi',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ], [
                'name' => 'Achike Ofodile',
                'slug' => 'achike-ofodile',
                'email' => 'achike@kainetixsports.com',
                'password' => '$2y$10$qmOC7anO9c/gkwMjKFDsM.0P/K9oihtcMwkHM6l/FXSIMIlbcSxFi',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        );
        
        DB::table('abouts')->truncate();
        DB::table('expertises')->truncate();
        DB::table('admins')->truncate();

        DB::table('abouts')->insert($abouts);

        DB::table('expertises')->insert($expertises);

        DB::table('admins')->insert($admins);

        $faker = Faker::create();

        DB::table('events')->truncate();    

        $folder = public_path('img/events');

        $files = glob($folder . '/*');
        
        foreach($files as $file){
            if(is_file($file)){

                $image = explode('/', $file);

                $title = $faker->sentence($nbWords = 4, $variableNbWords = false);

                DB::table('events')->insert([
                    'title' => $title,
                    'slug' => Str::slug($title, '-'),
                    'description' => $faker->text($maxNbChars = 9000),
                    'image' => $image[5] ?? $image[2],
                    'venue' => $faker->text($maxNbChars = 20),
                    'time' => Carbon::parse($faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now'))->toDateTimeString(),
                    'created_at' => Carbon::parse($faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now'))->toDateTimeString(),
                    'updated_at' => Carbon::parse($faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now'))->toDateTimeString()
                ]);
            }
        }

        DB::table('galleries')->truncate();

        $folder = public_path('img/gallery');

        $files = glob($folder . '/*');
        
        foreach($files as $file){
            if(is_file($file)){

                $image = explode('/', $file);

                $title = $faker->sentence($nbWords = 4, $variableNbWords = false);

                DB::table('galleries')->insert([
                    'title' => $title,
                    'slug' => Str::slug($title, '-'),
                    'description' => $faker->text($maxNbChars = 100),
                    'image' => $image[5] ?? $image[2],
                    'created_at' => Carbon::parse($faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now'))->toDateTimeString(),
                    'updated_at' => Carbon::parse($faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now'))->toDateTimeString()
                ]);
            }
        }

        DB::table('comments')->truncate();
        
        foreach (range(1,500) as $index) {

            DB::table('comments')->insert([
                'post_id' => $faker->numberBetween($min = 1, $max = 10),
                'name' => $faker->name,
                'email' => $faker->email,
                'comment' => $faker->text($maxNbChars = 90),
                'created_at' => Carbon::parse($faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'))->toDateTimeString(),
                'updated_at' => Carbon::parse($faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'))->toDateTimeString()
            ]);
        }

        DB::table('posts')->truncate();    


        $folder = public_path('img/news');

        $files = glob($folder . '/*');

        foreach($files as $file){
            if(is_file($file)){

                $image = explode('/', $file);

                $title = $faker->sentence($nbWords = 4, $variableNbWords = false);

                DB::table('posts')->insert([
                    'title' => $title,
                    'slug' => Str::slug($title, '-'),
                    'description' => $faker->text($maxNbChars = 900),
                    'image' => $image[5] ?? $image[2],
                    'visible' => true,
                    'admin_id' => 1,
                    'created_at' => Carbon::parse($faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now'))->toDateTimeString(),
                    'updated_at' => Carbon::parse($faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now'))->toDateTimeString()
                ]);
            }
        }

        DB::table('talents')->truncate();   

        DB::table('talents')->insert([
            'slug' => 'eden-hazard',
            'name' => 'Eden Hazard',
            'images' => '["eden-hazard-0-1562094528.jpg","eden-hazard-1-1562094528.jpg","eden-hazard-2-1562094528.jpg","eden-hazard-3-1562094528.jpg"]',
            'avatar' => 'eden-hazard-1562094528.jpg',
            'about' => "<p><span style='font-family: Arial;'>﻿</span><font face='Arial'>Eden Michael Hazard (French pronunciation: ​[edɛn azaʁ]; born 7 January 1991) is a Belgian professional footballer who plays as a winger or attacking midfielder for Real Madrid and captains the Belgium national team. Widely considered one of the best players in the world, Hazard is known for his creativity, speed, dribbling and passing.[4][5][6]<br></font></p><p><font face='Arial'><br></font></p><p><font face='Arial'>Hazard is the son of two former footballers and began his career in Belgium playing for local youth clubs. In 2005, he moved to France where he began his senior career, joining Ligue 1 club Lille. Hazard spent two years in the club's academy and, at the age of 16, made his professional debut in November 2007. He went on to become an integral part of the Lille team under manager Rudi Garcia, racking up over 190 appearances. In his first full season as a starter, he won the Ligue 1 Young Player of the Year award, becoming the first non-French player to win the award.[7] In the 2009–10 season, Hazard captured the award again, becoming the first player to win the award twice.[8] He was also named to the Ligue 1 Team of the Year. In the 2010–11 season, he was a part of the Lille team that won the league and cup double and, as a result of his performances, was named the Ligue 1 Player of the Year, the youngest player to win the award.[9] Hazard was also given the Bravo Award by Italian magazine Guerin Sportivo for his performances during the 2010–11 season.[10]</font></p><p><font face='Arial'><br></font></p><p><font face='Arial'>In June 2012 Hazard signed for English club Chelsea; he won the UEFA Europa League in his first season and the PFA Young Player of the Year in his second. In the 2014–15 season, he helped Chelsea win the League Cup and Premier League, earning him the FWA Footballer of the Year and the PFA Players' Player of the Year awards.[11] Two years later he won his second English league title as Chelsea won the 2016–17 Premier League. A year later he won the FA Cup, scoring in the final. In 2018, he was named in the FIFA FIFPro World XI. In June 2019, Chelsea agreed to a transfer fee of up to €130 million from Real Madrid for Hazard with the player signing a contract beginning on 1 July 2019.[12]</font></p><p><font face='Arial'><br></font></p><p><font face='Arial'>Hazard is a Belgium international, having represented his country at under-17 and under-19 level. Hazard made his senior international debut in November 2008, aged 17, in a friendly match against Luxembourg. Nearly three years after his debut, Hazard scored his first international goal against Kazakhstan in October 2011. He has since earned over 100 caps, and was a member of the Belgian squad which reached the quarter-finals of the 2014 FIFA World Cup and the UEFA Euro 2016. At the 2018 FIFA World Cup, he captained Belgium to third place which was their best finish in history, receiving the Silver Ball as the second best player of the tournament.</font></p>",
            'dob' => '2019-07-01',
            'created_at' => Carbon::now()->parse()->toDateTimeString(),
            'updated_at' => Carbon::now()->parse()->toDateTimeString()
        ]);

        
        DB::table('videos')->truncate();

        $video_ids = ['YPln3JP_gKs', 'O16RQWTNxaw', 'goI6JHO99Qg', 'FSs_JYwnAdI', 'QlR1LVMREf4', 'QCF7Q0iEMBw', '7Y8Ppin12r4', 'SSoJvyyvYpA', 'Uxex8CPIwCc', 'E2Bg2wB6hA8'];

        foreach($video_ids as $video){

            $title = $faker->sentence($nbWords = 4, $variableNbWords = false);

            DB::table('videos')->insert([
                'title' => $title,
                'slug' => Str::slug($title, '-'),
                'description' =>  $faker->text($maxNbChars = 900),
                'youtube_id' => $video,
                'created_at' => Carbon::parse($faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now'))->toDateTimeString(),
                'updated_at' => Carbon::parse($faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now'))->toDateTimeString()
            ]);
        }

    }

}
