//custom

$(document).ready(function () {
    $('.table').DataTable({
        dom: 'lfBrtpHF',
        buttons: [
            'excel', 'pdf', 'print'
        ],
        "pagingType": "simple_numbers"
    });

    deleteItem('deleteBtn')

    formValidation('form', 'formButton')

    $("#talentGallery").justifiedGallery();

    lightGallery('talentGallery', '.image', false, 'lg-slide-circular', null)

    $("#images").fileinput({
        allowedFileExtensions: ["jpg", "png", "gif", 'jpeg', 'bmp'],
        maxFileSize: 2000,
        maxFileCount: 20
    });

    var editTalentImage = $('#editImage')

    if (editTalentImage) {

        var preview = []
        var config = []

        var url = editTalentImage.data('url')
        var images = editTalentImage.data('images')
        var deleteUrl = editTalentImage.data('delete-url')

        if (images) {
            for (let image of images) {
                var imageURL = url + '/' + image
                
                preview.push(imageURL)

                config.push({
                    caption: image,
                    filename: image,
                    downloadUrl: imageURL,
                    width: '120px',
                    key: image
                })
            }
        }

        editTalentImage.fileinput({
            initialPreviewAsData: true,
            allowedFileExtensions: ["jpg", "png", "gif", 'jpeg', 'bmp'],
            maxFileSize: 2000,
            initialPreview: preview,
            initialPreviewConfig: config,
            maxFileCount: 20,
            deleteUrl: deleteUrl,
        });
    }


})

//light gallery
function lightGallery(id, selector, thumbnail, mode, cb) {
    $('#' + id).lightGallery({
        selector: selector,
        thumbnail: thumbnail,
        mode: mode,
        googlePlus: false,
        pinterest: false
    })
    setTimeout(cb, 100);
}

function deleteItem(btn) {
    $('.' + btn).each(function () {
        $(this).click(function (e) {
            e.preventDefault();
            if (confirm('Are you sure you want to delete this ? This action cannot be reversed')) {
                $(this).parent('form').submit()
            }
        })
    })
}

$('.dropify').dropify();

$('.datetime').datetimepicker();

$('.date').datetimepicker({
    format: 'MM/DD/YYYY'
});

$('.summernote').summernote({
    height: 200,
    followingToolbar: false,
    toolbar: [
        ['style', ['style', 'bold', 'italic', 'underline', 'clear', 'subscript', 'superscript']],
        ['font', ['strikethrough', 'superscript', 'subscript', 'fontname', 'fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['insert', ['picture', 'table', 'video', 'hr', 'link']],
        ['misc', ['fullscreen', 'codeview', 'undo', 'redo', 'help']]
    ],
    callbacks: {
        onImageUpload: function (files) {
            for (var i = files.length - 1; i >= 0; i--) {
                sendFile(files[i], $(this));
            }
        }
    }
});

function sendFile(file, summer) {
    let formData = new FormData();
    formData.append('image', file);
    axios.post("/admin/upload-image", formData)
        .then(function (response) {
            if (response['data']['status'] == true) {
                var url = response['data']['location'];
                summer.summernote('insertImage', url);
            } else {
                alert("Couldn't upload image, maybe the size is too big");
            }
        }).catch(function (error) {
            alert(error);
        });
}


$('.summernote').summernote('fontName', 'Arial');


//notifications
function notification(message, type) {
    var content = {};
    content.message = message;
    content.title = '';
    content.icon = 'fa fa-bell';

    $.notify(content, {
        type: type,
        placement: {
            from: 'bottom',
            align: 'right'
        },
        time: 1000,
    });
}

function formValidation(form, button) {
    var loader = $('#loader')
    var form = $('#' + form).parsley({
        errorsContainer: function (ParsleyField) {
            return ParsleyField.$element.attr("title");
        },
        errorsWrapper: false
    })

    $('#' + button).click(function (e) {
        if (form.isValid()) {
            e.preventDefault()
            $(this).addClass('hidden')
            if (loader) {
                loader.removeClass('hidden')
            }
            form.$element.submit()
        }
    })
}

window.Parsley.on('field:error', function (fieldInstance) {
    fieldInstance.$element.tooltip('dispose');
    fieldInstance.$element.tooltip({
        animation: true,
        container: 'body',
        placement: 'top',
        title: function () {
            return fieldInstance.getErrorsMessages().join(';');
        }
    });
});

window.Parsley.on('field:success', function (fieldInstance) {
    fieldInstance.$element.popover('dispose');
});

//admin js

$(function () {
    $('[data-toggle="tooltip"]').tooltip({
        html: true
    })
});

$(document).ready(function () {

    var toggle_sidebar = false,
        toggle_topbar = false,
        nav_open = 0,
        topbar_open = 0;

    if (!toggle_sidebar) {
        $toggle = $('.sidenav-toggler');

        $toggle.click(function () {
            if (nav_open == 1) {
                $('html').removeClass('nav_open');
                $toggle.removeClass('toggled');
                nav_open = 0;
            } else {
                $('html').addClass('nav_open');
                $toggle.addClass('toggled');
                nav_open = 1;
            }
        });
        toggle_sidebar = true;
    }

    if (!toggle_topbar) {
        $topbar = $('.topbar-toggler');

        $topbar.click(function () {
            if (topbar_open == 1) {
                $('html').removeClass('topbar_open');
                $topbar.removeClass('toggled');
                topbar_open = 0;
            } else {
                $('html').addClass('topbar_open');
                $topbar.addClass('toggled');
                topbar_open = 1;
            }
        });
        toggle_topbar = true;
    }
});



var Navbar = (function () {

    // Variables

    var $nav = $('.sidebar .nav');
    var $collapse = $('.sidebar .nav .collapse');
    var $dropdown = $('.sidebar .nav .dropdown');

    // Methods

    function accordion($this) {
        $this.closest($nav).find($collapse).not($this).collapse('hide');
    }

    function closeDropdown($this) {
        var $dropdownMenu = $this.find('.dashboard-dropdown-menu');

        $dropdownMenu.addClass('close');

        setTimeout(function () {
            $dropdownMenu.removeClass('close');
        }, 200);
    }


    // Events

    $collapse.on({
        'show.bs.collapse': function () {
            accordion($(this));
        }
    })

    $dropdown.on({
        'hide.bs.dropdown': function () {
            closeDropdown($(this));
        }
    })

})();


//
// Navbar collapse
//


var NavbarCollapse = (function () {

    // Variables

    var $nav = $('.navbar-nav'),
        $collapse = $('.navbar .collapse');


    // Methods

    function hideNavbarCollapse($this) {
        $this.addClass('collapsing-out');
    }

    function hiddenNavbarCollapse($this) {
        $this.removeClass('collapsing-out');
    }


    // Events

    if ($collapse.length) {
        $collapse.on({
            'hide.bs.collapse': function () {
                hideNavbarCollapse($collapse);
            }
        })

        $collapse.on({
            'hidden.bs.collapse': function () {
                hiddenNavbarCollapse($collapse);
            }
        })
    }

})();
