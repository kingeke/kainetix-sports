$(document).ready(function () {

    //wow animations
    new WOW().init();

    particlesJS.load('particles-js', '/js/particles.json');

    //gallery
    $("#mediaGallery").justifiedGallery({
        rowHeight: 200
    });

    $("#homeGallery").justifiedGallery({
        rowHeight: 100
    });

    $(".talentGallery").each(function () {       
        $(this).lightGallery({
            selector: '.image',
            thumbnail: false,
            mode: 'lg-scale-up',
            googlePlus: false,
            pinterest: false
        })
    })

    $('.talent-carousel').owlCarousel({
        autoHeight: true,
        loop: true,
        autoplay: true,
        autoplayTimeout: 2000,
        autoplayHoverPause: true,
        nav: true,
        navText: ['<i class="fas fa-arrow-circle-left fa-2x text-primary"></i>', '<i class="fas fa-arrow-circle-right fa-2x text-primary"></i>'],
        responsive:{
            0: {
                items: 1
            },  
            678:{
                items: 2,
            }
        }
    })

    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        videoHeight: 300,
        videoWidth: '100%',
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        nav: true,
        navText: ['<i class="fas fa-arrow-circle-left fa-2x text-primary"></i>', '<i class="fas fa-arrow-circle-right fa-2x text-primary"></i>'],
        video: true,
        center: true,
        responsive: {
            0: {
                items: 1
            }
        }
    })

    lightGallery('mediaGallery', '.image', true, 'lg-slide-circular', null)
    lightGallery('homeGallery', '.image', true, 'lg-slide-circular', null)
    lightGallery('lightGallery', '.item', true, 'lg-slide-circular', null)
    lightGallery('talentGallery', '.image', true, 'lg-scale-up', () => {
        $('#talent-modal').modal('hide')
    })

    //parsley validation
    formValidation()
});

function formValidation() {
    $('.form').each(function () {

        var loader = $(this).find('img')

        var form = $(this).parsley({
            errorsContainer: function (ParsleyField) {
                return ParsleyField.$element.attr("title");
            },
            errorsWrapper: false
        })

        $(this).find('button').click(function (e) {
            if (form.isValid()) {
                e.preventDefault()
                $(this).addClass('hidden')
                if (loader) {
                    loader.removeClass('hidden')
                }
                $(this).parents('form').submit();
            }
        })
    })
}


//light gallery
function lightGallery(id, selector, thumbnail, mode, cb) {
    $('#' + id).lightGallery({
        selector: selector,
        thumbnail: thumbnail,
        mode: mode,
        googlePlus: false,
        pinterest: false
    })
    setTimeout(cb, 100);
}

//notifications
function notification(message, type) {
    var content = {};
    content.message = message;
    content.title = '';
    content.icon = 'fa fa-bell';

    $.notify(content, {
        type: type,
        placement: {
            from: 'bottom',
            align: 'right'
        },
        time: 1000,
    });
}

window.Parsley.on('field:error', function (fieldInstance) {
    fieldInstance.$element.tooltip('dispose');
    fieldInstance.$element.tooltip({
        animation: true,
        container: 'body',
        placement: 'top',
        title: function () {
            return fieldInstance.getErrorsMessages().join(';');
        }
    });
});

window.Parsley.on('field:success', function (fieldInstance) {
    fieldInstance.$element.popover('dispose');
});