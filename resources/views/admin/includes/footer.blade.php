</body>
<script src="{{ asset('lib/bootstrap/popper.min.js') }}"></script>
<script src="{{ asset('lib/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('lib/axios/axios.js') }}"></script>
<script src="{{ asset('lib/parsley/parsley.min.js') }}"></script>
<script src="{{ asset('lib/notify/notify.min.js') }}"></script>
<script src="{{ asset('lib/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('lib/summernote/summernote-bs4.js') }}"></script>
<script src="{{ asset('lib/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('lib/dropify/dist/js/dropify.min.js') }}"></script>
<script src="{{ asset('lib/datetimepicker/moment.js') }}"></script>
<script src="{{ asset('lib/datetimepicker/datetimepicker.js') }}"></script>
<script src="{{ asset('lib/fileInput/js/fileinput.js') }}"></script>
<script src="{{ asset('lib/justifiedGallery/justifiedGallery.min.js') }}"></script>
<script src="{{ asset('lib/lightGallery-master/dist/js/lightgallery-all.js') }}"></script>
<script src="{{ asset('js/admin.js') }}"></script>

</html>
