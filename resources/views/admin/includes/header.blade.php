<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="{{ asset('img/website/favicon.png') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('lib/bootstrap/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/summernote/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/dropify/dist/css/dropify.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/datetimepicker/datetimepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/fileInput/css/fileinput.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/lightGallery-master/dist/css/lightgallery.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/lightGallery-master/dist/css/lg-transitions.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/justifiedGallery/justifiedGallery.min.css') }}">
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin-custom.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/fontawesome/css/all.min.css') }}">
    <script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
    <title>Kainetix Sports - Admin Dashboard</title>
</head>

<body>
    @include('includes.notifications')
