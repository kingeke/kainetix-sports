<div class="sidebar">
    <div class="scrollbar-inner sidebar-wrapper">
        <ul class="nav accordion" id="side-nav">
            <li class="nav-item">
                <a href="{{ route('admin-dashboard') }}" class="{{ Request::is('admin') ? 'active' : '' }}">
                    <i class=" fas fa-tachometer-alt text-success"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#blog-sub-menu" data-toggle="collapse" aria-expanded="false"
                    data-parent="#side-nav">
                    <i class="fas fa-photo-video text-info"></i> Media
                </a>
                <div class="text-center collapse container" id="blog-sub-menu">
                    <a href="{{ route('media-events-view') }}"
                        class="sub-nav-item {{ Request::is('admin/media/events*') ? 'active' : '' }}">
                        <i class="fas fa-calendar-alt text-primary"></i>
                        <p>Events</p>
                    </a>
                    <a href="{{ route('media-gallery-view') }}"
                        class="sub-nav-item {{ Request::is('admin/media/gallery*') ? 'active' : '' }}">
                        <i class=" fas fa-image text-dark"></i>
                        <p>Gallery</p>
                    </a>
                    <a href="{{ route('media-posts-view') }}"
                        class="sub-nav-item {{ Request::is('admin/media/posts*') ? 'active' : '' }}">
                        <i class="fas fa-newspaper text-success"></i>
                        <p>News</p>
                    </a>
                    <a href="{{ route('media-videos-view') }}"
                        class="sub-nav-item {{ Request::is('admin/media/videos*') ? 'active' : '' }}">
                        <i class="fas fa-video text-danger"></i>
                        <p>Videos</p>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#talents-sub-menu" data-toggle="collapse" aria-expanded="false"
                    data-parent="#side-nav">
                    <i class="fas fa-users text-dark"></i> Talents
                </a>
                <div class="text-center collapse container" id="talents-sub-menu">
                    <a href="{{ route('create-talent') }}"
                        class="sub-nav-item {{ Request::is('admin/talent/create') ? 'active' : '' }}">
                        <i class="fas fa-user-plus text-info"></i>
                        <p>Create</p>
                    </a>
                    <a href="{{ route('view-talents') }}"
                        class="sub-nav-item {{ Request::is('admin/talents/*') ? 'active' : '' }}">
                        <i class="fas fa-eye text-success"></i>
                        <p>View All</p>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#website-sub-menu" data-toggle="collapse" aria-expanded="false"
                    data-parent="#side-nav">
                    <i class="fas fa-cogs text-primary"></i> Wesbite
                </a>
                <div class="text-center collapse container" id="website-sub-menu">
                    <a href="{{ route('website-about-view') }}"
                        class="sub-nav-item {{ Request::is('admin/website/about*') ? 'active' : '' }}">
                        <i class="fas fa-question-circle text-success"></i>
                        <p>About</p>
                    </a>
                    <a href="{{ route('website-expertise-view') }}"
                        class="sub-nav-item {{ Request::is('admin/website/expertise*') ? 'active' : '' }}">
                        <i class="fas fa-sticky-note text-dark"></i>
                        <p>Our Expertise</p>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#admin-sub-menu" data-toggle="collapse" aria-expanded="false"
                    data-parent="#side-nav">
                    <i class="fas fa-user text-warning"></i> Admins
                </a>
                <div class="text-center collapse container" id="admin-sub-menu">
                    <a href="{{ route('create-admin') }}"
                        class="sub-nav-item {{ Request::is('admin/authorized-admin/create') ? 'active' : '' }}">
                        <i class="fas fa-user-plus text-info"></i>
                        <p>Create</p>
                    </a>
                    <a href="{{ route('view-admins') }}"
                        class="sub-nav-item {{ Request::is('admin/authorized-admins*') ? 'active' : '' }}"">
                        <i class=" fas fa-eye text-success"></i>
                        <p>View Admins</p>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</div>
