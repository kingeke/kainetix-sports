<div class="main-header">
    <div class="logo-header">
        <a href="{{ route('admin-dashboard') }}" class="logo">
            <img src="{{ asset('img/website/logo.jpg') }}" alt="Kainetix Sports" class="img-fluid" width="80">
            <small> Admin Dashboard</small>
        </a>
        <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse"
            data-target="collapse" aria-controls="sidebar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <button class="topbar-toggler more"><i class="la la-ellipsis-v"></i></button>
    </div>
    <nav class="navbar navbar-header navbar-expand-lg">
        <div class="container-fluid">
            <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
                        <img src='{{ asset('img/website/favicon.png') }}' alt="user-img" width="36" class="img-circle">
                        <span>{{ $admin->name }}</span>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <div class="user-box">
                                <div class="u-img">
                                    <img src="{{ asset('img/website/favicon.png') }}" alt="user">
                                </div>
                                <div class="u-text">
                                    <h4>{{ $admin->name }}</h4>
                                    <p class="text-muted">{{ $admin->email }}</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown-divider"></div>
                            <form action="{{ route('logout') }}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-link dropdown-item" href="#">
                                    <i class="fas fa-sign-out-alt text-primary"></i> Logout
                                </button>
                            </form>
                        </li>
                    </ul>
            </ul>
        </div>
    </nav>
</div>
