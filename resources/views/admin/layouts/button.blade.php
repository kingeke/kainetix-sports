<div class="text-center mb-3 mt-3">
    <img src="{{ asset('img/website/loader.gif') }}" alt="loader" width="30" class="hidden" id="loader">
    <button type="submit" id="formButton"
        class="{{ $buttonType == 'create' ? 'btn btn-outline-primary' : 'btn btn-outline-success' }}"><i
            class="{{ $buttonType == 'create' ? 'fas fa-plus' : 'fas fa-upload' }}"></i>
        {{ $buttonType == 'create' ? 'Create' : 'Update' }}</button>
</div>
