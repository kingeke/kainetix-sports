<div class="{{ $col ?? 'col-md-6' }}">
    <div class="form-group">
        @if($inputType != 'checkbox')
        <label class="form-control-label">{{ $label }}: @if($required)<span class="text-danger">*</span>@endif</label>
        @endif
        @if($inputType == 'textBox')
        <input type="{{$type ?? 'text' }}" class="form-control {{$class ?? ''}}" name="{{$name}}" id="{{$id ?? $name}}"
            placeholder="{{ $placeholder ?? 'Enter ' . $name }}" {{ $required ? ('required') : null }}
            value="{{ !empty($value) ? $value : (old($name) ? old($name) : (!empty($old) ? old($old) : null)) }}"
            minLength="{{ $minlength ?? '3' }}"
            {{ !empty($confirmationName) ? ('data-parsley-equalto=#' . $confirmationName) : null }} />
        @endif
        @if($inputType == 'dropify')
        <input type="file" name="{{$name}}" class="dropify" data-max-file-size-preview="2M"
            data-allowed-file-extensions="{{ $dropifyType == 'image' ? 'jpg jpeg gif png bmp' : ''}}"
            data-default-file="{{ $defaultImage ?? '' }}" {{ $required ? ('required') : null }}>
        @if($withRemoveButton ?? false)
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="{{$removeName}}" data-select="checkbox">
                <span class="form-check-sign">{{$removeLabel}}</span>
            </label>
        </div>
        @endif
        @endif
        @if($inputType == 'checkbox')
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="{{$name}}" data-select="checkbox"
                    {{ !empty($checked) ? ('checked') : null }}>
                <span class="form-check-sign">{{$label}}</span>
            </label>
        </div>
        @endif
        @if($inputType == 'summernote')
        <textarea class="form-control summernote" name="{{$name}}" rows="5"
            placeholder="{{ $placeholder ?? 'Enter ' . $name }}"
            {{ $required ? ('required') : null }}>{!! $value ?? old($name) !!}</textarea>
        @endif
        @if($inputType == 'textarea')
        <textarea class="form-control" name="{{$name}}" rows="{{ $rows ?? '5' }}"
            placeholder="{{ $placeholder ?? 'Enter ' . $name }}"
            {{ $required ? ('required') : null }}>{!! $value ?? old($name) !!}</textarea>
        @endif
    </div>
</div>
