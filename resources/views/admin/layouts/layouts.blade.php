@if($type == 'col')
<div class="{{ $col ?? 'col-md-6' }}">
    <div class="form-group">
        <label class="form-control-label">{{ $title }}:</label>
        <p>
            {!! $description !!}
        </p>
    </div>
</div>
@endif
