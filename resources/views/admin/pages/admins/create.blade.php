@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Admin - Create</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{ route('create-admin') }}" method="POST" id="form" class="pt-5">
                                    @csrf
                                    <div class="row">
                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Name',
                                        'required' => true,
                                        'name' => 'name',
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Email',
                                        'required' => true,
                                        'name' => 'email',
                                        'type' => 'email',
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Password',
                                        'required' => true,
                                        'name' => 'password',
                                        'type' => 'password',
                                        'minlength' => '6'
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Confirm Password',
                                        'required' => true,
                                        'name' => 'password_confirmation',
                                        'type' => 'password',
                                        'placeholder' => 'Enter password again',
                                        'minlength' => '6',
                                        'confirmationName' => 'password'
                                        ])

                                        <div class="col-12">
                                            @include('admin.layouts.button', ['buttonType' => 'create'])
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
