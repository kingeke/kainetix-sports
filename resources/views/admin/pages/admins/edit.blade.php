@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Edit Admin - {{ $theAdmin->name }}</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if($type == 'info')
                                <form action="{{ route('edit-admin', [$theAdmin->slug, 'info']) }}" method="POST"
                                    id="form" class="pt-5">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Name',
                                        'required' => true,
                                        'name' => 'name',
                                        'value' => $theAdmin->name
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Email',
                                        'required' => true,
                                        'name' => 'email',
                                        'type' => 'email',
                                        'value' => $theAdmin->email
                                        ])

                                        <div class="col-12">
                                            @include('admin.layouts.button', ['buttonType' => 'edit'])
                                        </div>
                                    </div>
                                </form>
                                @else
                                <form action="{{ route('edit-admin', [$theAdmin->slug, 'password']) }}" method="POST"
                                    id="form" class="pt-5">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Password',
                                        'required' => true,
                                        'name' => 'password',
                                        'type' => 'password',
                                        'minlength' => '6'
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Confirm Password',
                                        'required' => true,
                                        'name' => 'password_confirmation',
                                        'type' => 'password',
                                        'placeholder' => 'Enter password again',
                                        'minlength' => '6',
                                        'confirmationName' => 'password'
                                        ])
                                        <div class="col-12">
                                            @include('admin.layouts.button', ['buttonType' => 'edit'])
                                        </div>
                                    </div>
                                </form>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
