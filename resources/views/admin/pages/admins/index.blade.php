@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Admins</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-6 mb-3">
                                        <p class="card-category">Edit the admins</p>
                                    </div>
                                    <div class="col-sm-6 text-sm-right">
                                        <a href="{{ route('create-admin') }}" class="btn btn-primary text-white"><i
                                                class="fas fa-plus"></i> Create
                                            New</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>No of Posts</th>
                                                <th>Joined On</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($admins as $theAdmin)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $theAdmin->name }}</td>
                                                <td>{{ $theAdmin->email }}</td>
                                                <td>{{ $theAdmin->posts->count() }}</td>
                                                <td>{{ $theAdmin->created_at->format('jS M, Y - h:ia') }}</td>
                                                <td>
                                                    <div class="form-button-action">
                                                        <a href="{{ route('edit-admin', [$theAdmin->slug, 'info']) }}"
                                                            data-toggle="tooltip" title="Edit Admin Info"
                                                            class="btn btn-link">
                                                            <i class="fas fa-edit"></i>
                                                        </a>
                                                        <a href="{{ route('edit-admin', [$theAdmin->slug, 'password']) }}"
                                                            data-toggle="tooltip" title="Edit Admin Password"
                                                            class="btn btn-link btn-info">
                                                            <i class="fas fa-key"></i>
                                                        </a>
                                                        @if($theAdmin->email != $admin->email)
                                                        <form action="{{ route('delete-admin', $theAdmin->slug) }}"
                                                            method="POST" class="deleteForm">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="button" data-toggle="tooltip" title="Delete"
                                                                class="btn btn-link btn-danger deleteBtn">
                                                                <i class="fas fa-trash"></i>
                                                            </button>
                                                        </form>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
