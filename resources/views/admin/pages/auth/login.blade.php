@include('admin.includes.header')
<div class="wrapper my-auto" style="background: linear-gradient(135deg, #3667b6 0%, #5f62a5 50%, #6c757d 100%);">
    <div class="row h-100" style="margin: 0px !important">
        <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-12 my-auto">
            <div class="card shadow-lg rounded">
                <div class="card-body">
                    <form action="{{ route('login') }}" id="form" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-12 text-center">
                                <img src="{{ asset('img/website/logo.jpg') }}" alt="Kainetix Sports" class="img-fluid">
                                <p>Admin Login</p>
                            </div>
                            @include('admin.layouts.inputs',[
                            'inputType' => 'textBox',
                            'label' => 'Email',
                            'required' => true,
                            'name' => 'email',
                            'col' => 'col-12'
                            ])

                            @include('admin.layouts.inputs',[
                            'inputType' => 'textBox',
                            'label' => 'Password',
                            'required' => true,
                            'name' => 'password',
                            'type' => 'password',
                            'col' => 'col-12'
                            ])
                            
                            <div class="col-12 text-right">
                                <a href="{{ route('forgot-password') }}">Forgot Password ?</a>
                            </div>
                            <div class="col-12 text-center mt-3 mb-3">
                                <img src="{{ asset('img/website/loader.gif') }}" alt="loader" width="30" class="hidden"
                                    id="loader">
                                <button type="submit" id="formButton"
                                    class="btn btn-round btn-lg btn-outline-success">Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.includes.footer')
