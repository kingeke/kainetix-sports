@include('admin.includes.header')
<div class="wrapper my-auto" style="background: linear-gradient(135deg, #3667b6 0%, #5f62a5 50%, #6c757d 100%);">
    <div class="row h-100" style="margin: 0px !important">
        <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-12 my-auto">
            <div class="card shadow-lg rounded">
                <div class="card-body">
                    <form action="{{ route('password-reset-post') }}" method="POST" id="form">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="row">
                            <div class="col-12 text-center">
                                <img src="{{ asset('img/website/logo.jpg') }}" alt="Kainetix Sports" class="img-fluid">
                                <p>Reset Your Password</p>
                            </div>

                            @include('admin.layouts.inputs',[
                            'inputType' => 'textBox',
                            'label' => 'Email',
                            'required' => true,
                            'name' => 'email',
                            'type' => 'email',
                            'col' => 'col-12'
                            ])

                            @include('admin.layouts.inputs',[
                            'inputType' => 'textBox',
                            'label' => 'Password',
                            'required' => true,
                            'name' => 'password',
                            'type' => 'password',
                            'col' => 'col-12'
                            ])

                            @include('admin.layouts.inputs',[
                            'inputType' => 'textBox',
                            'label' => 'Confirm Password',
                            'required' => true,
                            'name' => 'password_confirmation',
                            'type' => 'password',
                            'placeholder' => 'Enter password again',
                            'minlength' => '6',
                            'confirmationName' => 'password',
                            'col' => 'col-12'
                            ])

                            <div class="col-12 text-center mt-3 mb-3">
                                <img src="{{ asset('img/website/loader.gif') }}" alt="loader" width="30" class="hidden"
                                    id="loader">
                                <button type="submit" id="formButton"
                                    class="btn btn-round btn-lg btn-outline-success">Reset Password</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.includes.footer')
