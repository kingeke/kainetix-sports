@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Dashboard</h4>
                <div class="row">
                    <div class="col-md-3">
                        <a href="{{ route('media-events-view') }}">
                            <div class="card card-stats card-warning shadow">
                                <div class="card-body ">
                                    <div class="row">
                                        <div class="col-5">
                                            <div class="icon-big text-center">
                                                <i class="fas fa-building"></i>
                                            </div>
                                        </div>
                                        <div class="col-7 d-flex align-items-center">
                                            <div class="numbers">
                                                <p class="card-category">Events</p>
                                                <h4 class="card-title">{{ number_format($events->total()) }}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <div class="card card-stats card-success shadow">
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-5">
                                        <div class="icon-big text-center">
                                            <i class="fas fa-photo-video"></i>
                                        </div>
                                    </div>
                                    <div class="col-7 d-flex align-items-center">
                                        <div class="numbers">
                                            <p class="card-category"><a href="{{ route('media-gallery-view') }}"
                                                    class="text-white">Photo</a> / <a
                                                    href="{{ route('media-videos-view') }}"
                                                    class="text-white">Videos</a></p>
                                            <h4 class="card-title">{{ number_format($media) }}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ route('media-posts-view') }}">
                            <div class="card card-stats card-danger shadow">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-5">
                                            <div class="icon-big text-center">
                                                <i class="fas fa-newspaper"></i>
                                            </div>
                                        </div>
                                        <div class="col-7 d-flex align-items-center">
                                            <div class="numbers">
                                                <p class="card-category">News</p>
                                                <h4 class="card-title">{{ number_format($posts->total()) }}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ route('view-talents') }}">
                            <div class="card card-stats card-primary shadow">
                                <div class="card-body ">
                                    <div class="row">
                                        <div class="col-5">
                                            <div class="icon-big text-center">
                                                <i class="fas fa-users"></i>
                                            </div>
                                        </div>
                                        <div class="col-7 d-flex align-items-center">
                                            <div class="numbers">
                                                <p class="card-category">Talents</p>
                                                <h4 class="card-title">{{ number_format($talents->total()) }}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Events</h4>
                                <p class="card-category">List of Latest Events</p>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Title</th>
                                                <th>Venue</th>
                                                <th>Event Schedule</th>
                                                <th>Bookings</th>
                                                <th>Participants</th>
                                                <th>Created On</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($events as $event)
                                            @include('admin.tables.eventTableComponent', ['fields' => ['title', 'venue',
                                            'time', 'bookings', 'participants', 'created_at'], 'actions' => ['view', 'edit']])
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">News</h4>
                                <p class="card-category">List of Latest News Post</p>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Visible</th>
                                                <th>Comments</th>
                                                <th>Created By</th>
                                                <th>Created On</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($posts as $post)
                                            @include('admin.tables.postTableComponent', ['fields' => ['title',
                                            'description', 'visible', 'comments', 'created_by', 'created_at', ],
                                            'actions' => ['view', 'edit']])
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Talents</h4>
                                <p class="card-category">Latest Talents</p>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone No.</th>
                                                <th>Sport</th>
                                                <th>About</th>
                                                <th>DOB</th>
                                                <th>Avatar</th>
                                                <th>No of Images</th>
                                                <th>No of Socials</th>
                                                <th>Created At</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($talents as $talent)
                                            @include('admin.tables.talentTableComponent', ['fields' => ['name', 'email',
                                            'phone', 'sport', 'about', 'dob','avatar', 'images', 'socials',
                                            'created_at'],
                                            'actions' => ['view', 'edit']])
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
