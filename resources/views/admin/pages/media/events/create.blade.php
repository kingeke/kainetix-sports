@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Event - Create</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{ route('media-event-create') }}" method="POST" id="form" class="pt-5"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Title',
                                        'required' => true,
                                        'name' => 'title',
                                        'col' => 'col-md-4'
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Event Schedule',
                                        'required' => true,
                                        'name' => 'time',
                                        'placeholder' => 'Enter the schedule for the event',
                                        'class' => 'datetime',
                                        'col' => 'col-md-4'])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'checkbox',
                                        'label' => 'Bookings Allowed?',
                                        'required' => true,
                                        'name' => 'bookings',
                                        'col' => 'col-md-4',
                                        'checked' => true
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Event Venue',
                                        'required' => true,
                                        'name' => 'venue',
                                        'placeholder' => 'Enter the venue for the event'])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'dropify',
                                        'label' => 'Lead Image: (Optional)',
                                        'required' => false,
                                        'name' => 'image',
                                        'dropifyType' => 'image'])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'summernote',
                                        'label' => 'Description',
                                        'required' => true,
                                        'name' => 'description',
                                        'col' => 'col-12'])
                                    </div>
                                    @include('admin.layouts.button', ['buttonType' => 'create'])
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
