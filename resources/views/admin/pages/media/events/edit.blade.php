@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Edit - {{ $event->title }}</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{ route('media-event-edit', $event->slug) }}" method="POST" id="form"
                                    class="pt-5" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Title',
                                        'required' => true,
                                        'name' => 'title',
                                        'value' => $event->title,
                                        'col' => 'col-md-4'])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Event Schedule',
                                        'required' => true,
                                        'name' => 'time',
                                        'value' => $event->time->format('m/d/Y h:ia'),
                                        'placeholder' => 'Enter the schedule for the event',
                                        'class' => 'datetime',
                                        'col' => 'col-md-4'])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'checkbox',
                                        'label' => 'Bookings Allowed?',
                                        'required' => true,
                                        'name' => 'bookings',
                                        'col' => 'col-md-4',
                                        'checked' => $event->bookings
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Event Venue',
                                        'required' => true,
                                        'name' => 'venue',
                                        'value' => $event->venue,
                                        'placeholder' => 'Enter the venue for the event'])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'dropify',
                                        'label' => 'Lead Image: (Optional)',
                                        'required' => false,
                                        'name' => 'image',
                                        'dropifyType' => 'image',
                                        'defaultImage' => $event->image ? asset('img/events/' . $event->image) : '',
                                        'withRemoveButton' => $event->image ? true : false,
                                        'removeName' => 'removeImage',
                                        'removeLabel' => 'Remove Image'])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'summernote',
                                        'label' => 'Description',
                                        'required' => true,
                                        'name' => 'description',
                                        'value' => $event->description,
                                        'col' => 'col-12'])

                                    </div>

                                    @include('admin.layouts.button', [
                                    'buttonType' => 'edit'])
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
