@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Media - Events <span
                        class="badge badge-success">{{ number_format($events->total()) }}</span></h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-6 mb-3">
                                        <p class="card-category">View/Create new events</p>
                                    </div>
                                    <div class="col-sm-6 text-sm-right">
                                        <a href="{{ route('media-event-create') }}"
                                            class="btn btn-primary text-white"><i class="fas fa-plus"></i> Create
                                            New</a>
                                    </div>
                                    <div class="col-12 mt-3">
                                        @if($events->hasMorePages())
                                        <p>Page</p>
                                        @endif
                                        {{ $events->links() }}
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Title</th>
                                                <th>Venue</th>
                                                <th>Event Schedule</th>
                                                <th>Bookings</th>
                                                <th>Participants</th>
                                                <th>Created On</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($events as $event)
                                            @include('admin.tables.eventTableComponent', ['fields' => ['title', 'venue',
                                            'time', 'bookings', 'participants', 'created_at'], 'actions' => ['view', 'edit',
                                            'delete']])
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
