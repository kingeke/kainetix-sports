@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Event - {{ $event->title }}</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 text-right my-auto">
                                        <div class="dropdown">
                                            <a class="btn btn-primary dropdown-toggle" href="#" role="button"
                                                id="actions" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                                Actions
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="actions">
                                                <a class="dropdown-item"
                                                    href="{{ route('media-event-edit', $event->slug) }}"><i
                                                        class="fa fa-edit text-warning"></i>
                                                    Edit Event
                                                </a>
                                                <form action="{{ route('media-event-delete', $event->slug) }}"
                                                    method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="dropdown-item deleteBtn" href=""><i
                                                            class="fas fa-trash text-danger"></i>
                                                        Delete Event
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        @if($event->image)
                                        <div class="col-12 text-center">
                                            <img src="{{ asset('img/events/' . $event->image) }}"
                                                class="img-fluid img-thumbnail" width="200">
                                        </div>
                                        @endif
                                        @include('admin.layouts.layouts', ['type' => 'col', 'title' => 'Title',
                                        'description' =>
                                        $event->title])
                                        @include('admin.layouts.layouts', ['type' => 'col', 'title' => 'Bookings Allowed?',
                                        'description' =>
                                        $event->bookings ? 'Yes' : 'No'])
                                        @include('admin.layouts.layouts', ['type' => 'col', 'title' => 'Description',
                                        'description' =>
                                        $event->description, 'col' => 'col-12'])
                                        @include('admin.layouts.layouts', ['type' => 'col', 'title' => 'Venue',
                                        'description' =>
                                        $event->venue])
                                        @include('admin.layouts.layouts', ['type' => 'col', 'title' => 'Event Schedule',
                                        'description' =>
                                        $event->time->format('jS M, Y - h:ia')])
                                        @include('admin.layouts.layouts', ['type' => 'col', 'title' => 'Created At',
                                        'description' =>
                                        $event->created_at->format('jS M, Y - h:ia')])
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h4 class="page-title">Participants - <span
                        class="badge badge-warning">{{ number_format($participants->total()) }}</span></h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12 mt-3">
                                        @if($participants->hasMorePages())
                                        <p>Page</p>
                                        @endif
                                        {{ $participants->links() }}
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Number</th>
                                                <th>Booked On</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($participants as $participant)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $participant->name }}</td>
                                                <td>{{ $participant->email }}</td>
                                                <td>{{ $participant->number }}</td>
                                                <td>{{ $participant->created_at->format('jS M, Y - h:ia') }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
