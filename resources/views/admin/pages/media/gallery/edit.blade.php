@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Edit Image</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{ route('media-gallery-edit', $gallery->slug) }}" method="POST"
                                    id="form" class="pt-5" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Title',
                                        'required' => false,
                                        'name' => 'title',
                                        'value' => $gallery->title
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'dropify',
                                        'label' => 'Image',
                                        'required' => false,
                                        'name' => 'image',
                                        'dropifyType' => 'image',
                                        'defaultImage' => asset('img/gallery/' . $gallery->image),
                                        'withRemoveButton' => false 
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textarea',
                                        'label' => 'Description',
                                        'required' => false,
                                        'name' => 'description',
                                        'col' => 'col-12',
                                        'value' => $gallery->description
                                        ])

                                        <div class=" col-12">
                                            @include('admin.layouts.button', ['buttonType' => 'edit'])
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
