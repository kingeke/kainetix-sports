@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Media - Gallery <span
                        class="badge badge-success">{{ number_format($gallery->total()) }}</span></h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-6 mb-3">
                                        <p class="card-category">View our images</p>
                                    </div>
                                    <div class="col-sm-6 text-sm-right">
                                        <a href="{{ route('media-gallery-create') }}"
                                            class="btn btn-primary text-white"><i class="fas fa-plus"></i> Create
                                            New</a>
                                    </div>
                                    <div class="col-12 mt-3">
                                        @if($gallery->hasMorePages())
                                        <p>Page</p>
                                        @endif
                                        {{ $gallery->links() }}
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Image</th>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Created On</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($gallery as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td class="text-center">
                                                    <img src='{{ asset('img/gallery/' . $item->image) }}' width='100'
                                                        class="img-fluid img-thumbnail rounded" />
                                                </td>
                                                <td>{{ $item->title ?? 'Not Available' }}</td>
                                                <td>
                                                    {{ str_limit($item->description ?? 'Not Available', 100) }}</td>
                                                <td>{{ $item->created_at->format('jS M, Y - h:ia') }}</td>
                                                <td>
                                                    <div class="form-button-action">
                                                        <a href="{{ route('media-gallery-edit', $item->slug) }}"
                                                            data-toggle="tooltip" title="Edit" class="btn btn-link">
                                                            <i class="fas fa-edit"></i>
                                                        </a>
                                                        <form action="{{ route('media-gallery-delete', $item->slug) }}"
                                                            method="POST" class="deleteForm">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="button" data-toggle="tooltip" title="Delete"
                                                                class="btn btn-link btn-danger deleteBtn">
                                                                <i class="fas fa-trash"></i>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
