@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">News - Create</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{ route('media-post-create') }}" method="POST" id="form" class="pt-5"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Title',
                                        'required' => true,
                                        'name' => 'title',
                                        ])

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-control-label">Visible: <span
                                                        class="text-danger">*</span></label>
                                                <select name="visible" class="form-control">
                                                    <option value="Yes"
                                                        {{ old('visible') == 'Yes' ? 'selected="selected"' : '' }}>Yes
                                                    </option>
                                                    <option value="No"
                                                        {{ old('visible') == 'No' ? 'selected="selected"' : '' }}>No
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'dropify',
                                        'label' => 'Lead Image: (Optional)',
                                        'required' => false,
                                        'name' => 'image',
                                        'dropifyType' => 'image'])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'summernote',
                                        'label' => 'Description',
                                        'required' => true,
                                        'name' => 'description',
                                        'col' => 'col-12'])
                                    </div>
                                    @include('admin.layouts.button', ['buttonType' => 'create'])
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
