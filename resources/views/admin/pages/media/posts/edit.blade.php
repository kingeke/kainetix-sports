@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Edit - {{ $post->title }} </h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{ route('media-post-edit', $post->slug) }}" method="POST" id="form" class="pt-5"
                                    enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Title',
                                        'required' => true,
                                        'name' => 'title',
                                        'value' => $post->title
                                        ])

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-control-label">Visible: <span
                                                        class="text-danger">*</span></label>
                                                <select name="visible" class="form-control">
                                                    <option value="Yes"
                                                        {{ $post->visible == 'Yes' ? 'selected="selected"' : '' }}>Yes
                                                    </option>
                                                    <option value="No"
                                                        {{ $post->visible == 'No' ? 'selected="selected"' : '' }}>No
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'dropify',
                                        'label' => 'Lead Image: (Optional)',
                                        'required' => false,
                                        'name' => 'image',
                                        'dropifyType' => 'image',
                                        'defaultImage' => $post->image ? asset('img/news/' . $post->image) : '',
                                        'withRemoveButton' => $post->image ? true : false,
                                        'removeName' => 'removeImage',
                                        'removeLabel' => 'Remove Image'
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'summernote',
                                        'label' => 'Description',
                                        'required' => true,
                                        'name' => 'description',
                                        'col' => 'col-12',
                                        'value' => $post->description
                                        ])
                                    </div>
                                    @include('admin.layouts.button', ['buttonType' => 'update'])
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
