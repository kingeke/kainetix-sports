@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Media - News <span
                        class="badge badge-success">{{ number_format($posts->total()) }}</span></h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-6 mb-3">
                                        <p class="card-category">View/Create new news post</p>
                                    </div>
                                    <div class="col-sm-6 text-sm-right">
                                        <a href="{{ route('media-post-create') }}"
                                            class="btn btn-primary text-white"><i class="fas fa-plus"></i> Create
                                            New</a>
                                    </div>
                                    <div class="col-12 mt-3">
                                        @if($posts->hasMorePages())
                                        <p>Page</p>
                                        @endif
                                        {{ $posts->links() }}
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Visible</th>
                                                <th>Comments</th>
                                                <th>Created By</th>
                                                <th>Created On</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($posts as $post)
                                            @include('admin.tables.postTableComponent', ['fields' => ['title', 'description', 'visible', 'comments', 'created_by', 'created_at', ], 'actions' => ['view', 'edit',
                                            'delete']])
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
