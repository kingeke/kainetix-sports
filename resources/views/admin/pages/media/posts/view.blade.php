@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Post - {{ $post->title }}</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 text-right my-auto">
                                        <div class="dropdown">
                                            <a class="btn btn-primary dropdown-toggle" href="#" role="button"
                                                id="actions" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                                Actions
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="actions">
                                                <a class="dropdown-item"
                                                    href="{{ route('media-post-edit', $post->slug) }}"><i
                                                        class="fa fa-edit text-warning"></i>
                                                    Edit Post
                                                </a>
                                                <form action="{{ route('media-post-delete', $post->slug) }}"
                                                    method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="dropdown-item deleteBtn" href=""><i
                                                            class="fas fa-trash text-danger"></i>
                                                        Delete Post
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        @if($post->image)
                                        <div class="col-12 text-center">
                                            <img src="{{ asset('img/news/' . $post->image) }}"
                                                class="img-fluid img-thumbnail" width="200">
                                        </div>
                                        @endif
                                        @include('admin.layouts.layouts', ['type' => 'col', 'title' => 'Title',
                                        'description' =>
                                        $post->title])

                                        @include('admin.layouts.layouts', ['type' => 'col', 'title' => 'Visible',
                                        'description' =>
                                        $post->visible ? 'Yes' : 'No'])

                                        @include('admin.layouts.layouts', ['type' => 'col', 'title' => 'Description',
                                        'description' =>
                                        $post->description, 'col' => 'col-12'])

                                        @include('admin.layouts.layouts', ['type' => 'col', 'title' => 'Created by',
                                        'description' =>
                                        $post->admin->name ?? 'Kainetix Sports'])

                                        @include('admin.layouts.layouts', ['type' => 'col', 'title' => 'Created at',
                                        'description' =>
                                        $post->created_at->format('jS M, Y - h:ia')])
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h4 class="page-title">Comments - <span
                        class="badge badge-warning">{{ number_format($comments->total()) }}</span></h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12 mt-3">
                                        <p class="mb-3">All the post comments</p>
                                        @if($comments->hasMorePages())
                                        <p>Page</p>
                                        @endif
                                        {{ $comments->links() }}
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Comment</th>
                                                <th>Has Replies</th>
                                                <th>Created On</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($comments as $comment)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $comment->name }}</td>
                                                <td>{{ $comment->email }}</td>
                                                <td>{{ $comment->comment }}</td>
                                                <td>{{ $comment->replies->count() > 0 ? 'Yes (' . $comment->replies->count() . ')' : 'No' }}
                                                </td>
                                                <td>{{ $comment->created_at->format('jS M, Y - h:ia') }}</td>
                                                <td>
                                                    <div class="form-button-action">
                                                        <form
                                                            action="{{ route('media-post-delete-comment', $comment->id) }}"
                                                            method="POST" class="deleteForm">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="button" data-toggle="tooltip"
                                                                title="Delete Comment"
                                                                class="btn btn-link btn-danger deleteBtn">
                                                                <i class="fas fa-trash"></i>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
