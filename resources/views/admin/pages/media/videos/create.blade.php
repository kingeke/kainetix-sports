@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Videos - Create</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{ route('media-video-create') }}" method="POST" id="form" class="pt-5"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12 text-center">
                                        </div>
                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Title',
                                        'required' => true,
                                        'name' => 'title',
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Youtube Video ID',
                                        'required' => true,
                                        'name' => 'youtube_id',
                                        'placeholder' => 'Enter youtube ID (e.g brKw9KtNm04)'
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textarea',
                                        'label' => 'Description (Optional)',
                                        'required' => false,
                                        'name' => 'description',
                                        'col' => 'col-12'])
                                    </div>
                                    @include('admin.layouts.button', ['buttonType' => 'create'])
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
