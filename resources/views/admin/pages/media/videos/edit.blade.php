@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Edit - {{ $video->title }}</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{ route('media-video-edit', $video->slug) }}" method="POST" id="form"
                                    class="pt-5" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        <div class="col-12 text-center">
                                        </div>
                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Title',
                                        'required' => true,
                                        'name' => 'title',
                                        'value' => $video->title
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Youtube Video ID',
                                        'required' => true,
                                        'name' => 'youtube_id',
                                        'placeholder' => 'Enter youtube ID',
                                        'value' => $video->youtube_id
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textarea',
                                        'label' => 'Description (Optional)',
                                        'required' => false,
                                        'name' => 'description',
                                        'value' => $video->description,
                                        'col' => 'col-12'])
                                    </div>
                                    @include('admin.layouts.button', ['buttonType' => 'update'])
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
