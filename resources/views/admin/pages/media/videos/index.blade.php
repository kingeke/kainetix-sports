@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Media - Videos <span
                        class="badge badge-success">{{ number_format($videos->total()) }}</span></h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-6 mb-3">
                                        <p class="card-category">View/Create new videos</p>
                                    </div>
                                    <div class="col-sm-6 text-sm-right">
                                        <a href="{{ route('media-video-create') }}"
                                            class="btn btn-primary text-white"><i class="fas fa-plus"></i> Create
                                            New</a>
                                    </div>
                                    <div class="col-12 mt-3">
                                        @if($videos->hasMorePages())
                                        <p>Page</p>
                                        @endif
                                        {{ $videos->links() }}
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Youtube ID</th>
                                                <th>Created On</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($videos as $video)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $video->title }}</td>
                                                <td>{{ str_limit($video->description ?? 'Not Available', 80) }}</td>
                                                <td>{{ $video->youtube_id }}</td>
                                                <td>{{ $video->created_at->format('jS M, Y - h:ia') }}</td>
                                                <td>
                                                    <div class="form-button-action">
                                                        <a href="{{ route('media-video-edit', $video->slug) }}"
                                                            data-toggle="tooltip" title="Edit Video"
                                                            class="btn btn-link">
                                                            <i class="fas fa-edit"></i>
                                                        </a>
                                                        <form action="{{ route('media-video-delete', $video->slug) }}"
                                                            method="POST" class="deleteForm">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="button" data-toggle="tooltip"
                                                                title="Delete Video"
                                                                class="btn btn-link btn-danger deleteBtn">
                                                                <i class="fas fa-trash"></i>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
