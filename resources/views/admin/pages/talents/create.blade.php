@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Talent - Create</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{ route('create-talent') }}" method="POST" id="form" class="pt-5"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Name',
                                        'required' => true,
                                        'name' => 'name',
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Email (Optional)',
                                        'required' => false,
                                        'name' => 'email',
                                        'type' => 'email',
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Phone Number (Optional)',
                                        'required' => false,
                                        'name' => 'phone',
                                        'type' => 'tel',
                                        'placeholder' => 'Enter phone 080xxxxxxx',
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Sport (Optional)',
                                        'required' => false,
                                        'name' => 'sport',
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Date of Birth (Optional)',
                                        'required' => false,
                                        'name' => 'dob',
                                        'class' => 'date',
                                        'placeholder' => 'Enter date of birth'
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'dropify',
                                        'label' => 'Avatar: (Optional)',
                                        'required' => false,
                                        'name' => 'avatar',
                                        'dropifyType' => 'image'])

                                        <div class="col-12">
                                            <div class="form-group">
                                                <a class="btn btn-primary" data-toggle="collapse" href="#socials"
                                                    role="button" aria-expanded="false" aria-controls="collapseExample">
                                                    Social Links
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="collapse {{ !empty((array) old('socials')) ? 'show' : '' }}"
                                                id="socials">
                                                <div class="row">
                                                    @include('admin.layouts.inputs',[
                                                    'inputType' => 'textBox',
                                                    'label' => 'Facebook',
                                                    'required' => false,
                                                    'name' => 'socials[facebook]',
                                                    'old' => 'socials.facebook',
                                                    'col' => 'col-lg-3 col-md-6',
                                                    'placeholder' => 'Facebook (e.g https://facebook.com)',
                                                    'type' => 'url'
                                                    ])

                                                    @include('admin.layouts.inputs',[
                                                    'inputType' => 'textBox',
                                                    'label' => 'LinkedIn',
                                                    'required' => false,
                                                    'name' => 'socials[linkedIn]',
                                                    'old' => 'socials.linkedIn',
                                                    'col' => 'col-lg-3 col-md-6',
                                                    'placeholder' => 'LinkedIn (e.g https://linkedin.com)',
                                                    'type' => 'url'
                                                    ])

                                                    @include('admin.layouts.inputs',[
                                                    'inputType' => 'textBox',
                                                    'label' => 'Twitter',
                                                    'required' => false,
                                                    'name' => 'socials[twitter]',
                                                    'old' => 'socials.twitter',
                                                    'col' => 'col-lg-3 col-md-6',
                                                    'placeholder' => 'Twitter (e.g https://twitter.com)',
                                                    'type' => 'url'
                                                    ])

                                                    @include('admin.layouts.inputs',[
                                                    'inputType' => 'textBox',
                                                    'label' => 'Instagram',
                                                    'required' => false,
                                                    'name' => 'socials[instagram]',
                                                    'old' => 'socials.instagram',
                                                    'col' => 'col-lg-3 col-md-6',
                                                    'placeholder' => 'Instagram (e.g https://instagram.com)',
                                                    'type' => 'url'
                                                    ])
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="form-control-label">
                                                    Image(s) (Optional)
                                                </label>
                                                <input id="images" name="images[]" type="file" multiple
                                                    data-show-upload="false" data-show-caption="true" accept="image/*"
                                                    data-browse-on-zone-click="true">
                                                </span>
                                            </div>
                                        </div>

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'summernote',
                                        'label' => 'About',
                                        'required' => true,
                                        'name' => 'about',
                                        'rows' => '8',
                                        'col' => 'col-12'
                                        ])

                                        <div class="col-12">
                                            @include('admin.layouts.button', ['buttonType' => 'create'])
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
