@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Edit - {{ $talent->name }}</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{ route('edit-talent', $talent->slug) }}" method="POST" id="form"
                                    class="pt-5" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Name',
                                        'required' => true,
                                        'name' => 'name',
                                        'value' => $talent->name
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Email (Optional)',
                                        'required' => false,
                                        'name' => 'email',
                                        'type' => 'email',
                                        'value' => $talent->email
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Phone Number (Optional)',
                                        'required' => false,
                                        'name' => 'phone',
                                        'type' => 'tel',
                                        'placeholder' => 'Enter phone 080xxxxxxx',
                                        'value' => $talent->phone
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Sport (Optional)',
                                        'required' => false,
                                        'name' => 'sport',
                                        'value' => $talent->sport
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Date of Birth (Optional)',
                                        'required' => false,
                                        'name' => 'dob',
                                        'class' => 'date',
                                        'placeholder' => 'Enter date of birth',
                                        'value' => $talent->dob ? $talent->dob->format('m/d/Y') : null
                                        ])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'dropify',
                                        'label' => 'Avatar: (Optional)',
                                        'required' => false,
                                        'name' => 'avatar',
                                        'dropifyType' => 'image',
                                        'defaultImage' => $talent->avatar ? asset('img/talents/' . $talent->avatar) :
                                        '',
                                        'withRemoveButton' => $talent->avatar ? true : false,
                                        'removeName' => 'removeAvatar',
                                        'removeLabel' => 'Remove Avatar'
                                        ])

                                        <div class="col-12">
                                            <div class="form-group">
                                                <a class="btn btn-primary" data-toggle="collapse" href="#socials"
                                                    role="button" aria-expanded="false" aria-controls="collapseExample">
                                                    Social Links
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="collapse {{ !empty((array) $talent->socials) ? 'show' : '' }}"
                                                id="socials">
                                                <div class="row">
                                                    @include('admin.layouts.inputs',[
                                                    'inputType' => 'textBox',
                                                    'label' => 'Facebook',
                                                    'required' => false,
                                                    'name' => 'socials[facebook]',
                                                    'old' => 'socials.facebook',
                                                    'col' => 'col-lg-3 col-md-6',
                                                    'placeholder' => 'Facebook (e.g https://facebook.com)',
                                                    'type' => 'url',
                                                    'value' => $talent->socials['facebook'] ?? ''
                                                    ])

                                                    @include('admin.layouts.inputs',[
                                                    'inputType' => 'textBox',
                                                    'label' => 'LinkedIn',
                                                    'required' => false,
                                                    'name' => 'socials[linkedIn]',
                                                    'old' => 'socials.linkedIn',
                                                    'col' => 'col-lg-3 col-md-6',
                                                    'placeholder' => 'LinkedIn (e.g https://linkedin.com)',
                                                    'type' => 'url',
                                                    'value' => $talent->socials['linkedIn'] ?? ''
                                                    ])

                                                    @include('admin.layouts.inputs',[
                                                    'inputType' => 'textBox',
                                                    'label' => 'Twitter',
                                                    'required' => false,
                                                    'name' => 'socials[twitter]',
                                                    'old' => 'socials.twitter',
                                                    'col' => 'col-lg-3 col-md-6',
                                                    'placeholder' => 'Twitter (e.g https://twitter.com)',
                                                    'type' => 'url',
                                                    'value' => $talent->socials['twitter'] ?? ''
                                                    ])

                                                    @include('admin.layouts.inputs',[
                                                    'inputType' => 'textBox',
                                                    'label' => 'Instagram',
                                                    'required' => false,
                                                    'name' => 'socials[instagram]',
                                                    'old' => 'socials.instagram',
                                                    'col' => 'col-lg-3 col-md-6',
                                                    'placeholder' => 'Instagram (e.g https://instagram.com)',
                                                    'type' => 'url',
                                                    'value' => $talent->socials['instagram'] ?? ''
                                                    ])
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="form-control-label">
                                                    Add/Remove Image(s) (Optional)
                                                </label>
                                                @if(count((array) $talent->images) > 0)
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox"
                                                            name="removeImages" data-select="checkbox">
                                                        <span class="form-check-sign">Remove All</span>
                                                    </label>
                                                </div>
                                                @endif
                                                <input id="editImage" name="images[]" type="file" multiple
                                                    data-show-upload="false" data-show-caption="true" accept="image/*"
                                                    data-browse-on-zone-click="true"
                                                    data-url="{{ asset('img/talents/') }}"
                                                    data-images="{{ json_encode($talent->images) }}"
                                                    data-delete-url={{ route('delete-image', $talent->slug) }}>
                                                </span>
                                            </div>
                                        </div>

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'summernote',
                                        'label' => 'About',
                                        'required' => true,
                                        'name' => 'about',
                                        'col' => 'col-12',
                                        'value' => $talent->about
                                        ])

                                        <div class="col-12">
                                            @include('admin.layouts.button', ['buttonType' => 'edit'])
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
