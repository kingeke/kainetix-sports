@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Our Talents</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-6 mb-3">
                                        <p class="card-category">View our talented players</p>
                                    </div>
                                    <div class="col-sm-6 text-sm-right">
                                        <a href="{{ route('create-talent') }}" class="btn btn-primary text-white"><i
                                                class="fas fa-plus"></i> Create
                                            New</a>
                                    </div>
                                    <div class="col-12 mt-3">
                                        @if($talents->hasMorePages())
                                        <p>Page</p>
                                        @endif
                                        {{ $talents->links() }}
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone No.</th>
                                                <th>Sport</th>
                                                <th>About</th>
                                                <th>DOB</th>
                                                <th>Avatar</th>
                                                <th>No of Images</th>
                                                <th>No of Socials</th>
                                                <th>Created At</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($talents as $talent)
                                                @include('admin.tables.talentTableComponent', ['fields' => ['name', 'email', 'phone', 'sport', 'about', 'dob','avatar', 'images', 'socials', 'created_at'],
                                                'actions' => ['view', 'edit', 'delete']])
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
