@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Talent - {{ $talent->name }}</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 text-right my-auto">
                                        <div class="dropdown">
                                            <a class="btn btn-primary dropdown-toggle" href="#" role="button"
                                                id="actions" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                                Actions
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="actions">
                                                <a class="dropdown-item"
                                                    href="{{ route('edit-talent', $talent->slug) }}"><i
                                                        class="fa fa-edit text-warning"></i>
                                                    Edit
                                                </a>
                                                <form action="{{ route('delete-talent', $talent->slug) }}"
                                                    method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="dropdown-item deleteBtn" href=""><i
                                                            class="fas fa-trash text-danger"></i>
                                                        Delete
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        @if($talent->avatar)
                                        <div class="col-12 text-center">
                                            <img src="{{ asset('img/talents/' . $talent->avatar) }}"
                                                class="img-fluid img-thumbnail" width="200">
                                        </div>
                                        @endif

                                        @include('admin.layouts.layouts', ['type' => 'col', 'title' => 'Name',
                                        'description' =>
                                        $talent->name])

                                        @include('admin.layouts.layouts', ['type' => 'col', 'title' => 'Email',
                                        'description' =>
                                        $talent->email ?? 'Not Available'])

                                        @include('admin.layouts.layouts', ['type' => 'col', 'title' => 'Phone Number',
                                        'description' =>
                                        $talent->phone ?? 'Not Available'])

                                        @include('admin.layouts.layouts', ['type' => 'col', 'title' => 'Sport',
                                        'description' =>
                                        $talent->sport ?? 'Not Available'])

                                        @include('admin.layouts.layouts', ['type' => 'col', 'title' => 'About',
                                        'description' =>
                                        $talent->about, 'col' => 'col-12'])

                                        @include('admin.layouts.layouts', ['type' => 'col', 'title' => 'Date of Birth',
                                        'description' =>
                                        $talent->dob ? $talent->dob->format('jS M, Y') : 'Not Available'])

                                        @include('admin.layouts.layouts', ['type' => 'col', 'title' => 'Created at',
                                        'description' =>
                                        $talent->created_at->format('jS M, Y - h:ia')])

                                        @if(count((array) $talent->socials) > 0)
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="form-control-label">Socials:</label>
                                                <ol>
                                                    @foreach($talent->socials as $social => $link)
                                                    <li>
                                                        <p>{{ ucfirst($social) }} - <a href="{{ $link }}"
                                                                target="_blank">{{ $link }}</a></p>
                                                    </li>
                                                    @endforeach
                                                </ol>
                                            </div>
                                        </div>
                                        @endif
                                        @if(count((array) $talent->images) > 0)
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label class="form-control-label">Other Image(s):</label>
                                                <div class="row no-gutters" id="talentGallery">
                                                    @foreach($talent->images as $image)
                                                    <div class="col-12">
                                                        <a class="image" data-dismiss="modal"
                                                            href="{{ asset('img/talents/' . $image) }}"
                                                            data-tweet-text="Hey guys, meet {{ $talent->name }} on Kainetix Sports">
                                                            <img src="{{ asset('img/talents/' . $image) }}"
                                                                class="img-fluid img-thumbnail" />
                                                            <div class="view">
                                                                <i class="fas fa-search"></i>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
