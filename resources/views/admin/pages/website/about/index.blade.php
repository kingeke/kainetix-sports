@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">About</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-6 mb-3">
                                        <p class="card-category">View our about information</p>
                                    </div>
                                    <div class="col-sm-6 text-sm-right">
                                        <a href="{{ route('website-about-create') }}"
                                            class="btn btn-primary text-white"><i class="fas fa-plus"></i> Create
                                            New</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Title</th>
                                                <th>Order</th>
                                                <th>Description</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($aboutContent as $about)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $about->title }}</td>
                                                <td>{{ $about->order }}</td>
                                                <td>
                                                    {{ str_limit(strip_tags($about->description), 100) }}</td>
                                                <td>
                                                    <div class="form-button-action">
                                                        <a href="{{ route('about') }}" target="_blank"
                                                            data-toggle="tooltip" title="View"
                                                            class="btn btn-link btn-success">
                                                            <i class="fas fa-eye"></i>
                                                        </a>
                                                        <a href="{{ route('website-about-edit', $about->slug) }}"
                                                            data-toggle="tooltip" title="Edit" class="btn btn-link">
                                                            <i class="fas fa-edit"></i>
                                                        </a>
                                                        <form action="{{ route('website-about-delete', $about->slug) }}"
                                                            method="POST" class="deleteForm">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="button" data-toggle="tooltip" title="Delete" class="btn btn-link btn-danger deleteBtn">
                                                                <i class="fas fa-trash"></i>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
