@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Expertise - Create</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{ route('website-expertise-create') }}" method="POST" id="form"
                                    class="pt-5" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Title',
                                        'required' => true,
                                        'name' => 'title',
                                        ])

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-control-label">Order: <span
                                                        class="text-danger">*</span> <small>To change the
                                                        arrangement</small></label>
                                                <select name="order" class="form-control">
                                                    @for($order = $expertiseLength + 1; $order >= 1; $order--) <option
                                                        value="{{$order}}"
                                                        {{ old('order') == $order ? 'selected="selected"' : '' }}>
                                                        {{$order}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                        
                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'dropify',
                                        'label' => 'Image: (Optional)',
                                        'required' => false,
                                        'name' => 'image',
                                        'dropifyType' => 'image'])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'summernote',
                                        'label' => 'Description',
                                        'required' => true,
                                        'name' => 'description',
                                        'col' => 'col-12'])

                                        <div class="col-12">
                                            @include('admin.layouts.button', ['buttonType' => 'create'])
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
