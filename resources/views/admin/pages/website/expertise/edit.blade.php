@include('admin.includes.header')
<div class="wrapper">
    @include('admin.includes.subHeader')
    @include('admin.includes.navigation')
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <h4 class="page-title">Edit - {{ $expertise->title }}</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{ route('website-expertise-edit', $expertise->slug) }}" method="POST"
                                    id="form" class="pt-5" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'textBox',
                                        'label' => 'Title',
                                        'required' => true,
                                        'name' => 'title',
                                        'value' => $expertise->title
                                        ])

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-control-label">Order: <span
                                                        class="text-danger">*</span> <small>To change the
                                                        arrangement</small></label>
                                                <select name="order" class="form-control">
                                                    @for($order = 1; $order <= $expertiseLength + 1; $order ++) <option
                                                        value="{{$order}}"
                                                        {{ $expertise->order == $order ? 'selected="selected"' : '' }}>
                                                        {{$order}}</option>
                                                        @endfor
                                                </select>
                                            </div>
                                        </div>

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'dropify',
                                        'label' => 'Lead Image: (Optional)',
                                        'required' => false,
                                        'name' => 'image',
                                        'dropifyType' => 'image',
                                        'defaultImage' => $expertise->image ? asset('img/website/expertise/' .
                                        $expertise->image) : '',
                                        'withRemoveButton' => $expertise->image ? true : false,
                                        'removeName' => 'removeImage',
                                        'removeLabel' => 'Remove Image'])

                                        @include('admin.layouts.inputs',[
                                        'inputType' => 'summernote',
                                        'label' => 'Description',
                                        'required' => true,
                                        'name' => 'description',
                                        'col' => 'col-12',
                                        'value' => $expertise->description
                                        ])

                                        <div class=" col-12">
                                            @include('admin.layouts.button', ['buttonType' => 'edit'])
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
