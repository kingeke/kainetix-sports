<tr>
    <td>{{ $loop->iteration }}</td>
    @foreach($fields as $field)
        @if($field == 'title')
            <td>{{ $event->title }}</td>
        @endif
        @if($field == 'venue')
        <td>{{ $event->venue }}</td>
        @endif
        @if($field == 'time')
        <td>{{ $event->time->format('jS M, Y - h:ia') }}</td>
        @endif
        @if($field == 'bookings')
        <td>{{ $event->bookings ? 'Enabled' : 'Disabled' }}</td>
        @endif
        @if($field == 'participants')
        <td>{{ number_format($event->participants->count()) }}</td>
        @endif
        @if($field == 'created_at')
        <td>{{ $event->created_at->format('jS M, Y - h:ia') }}</td>
        @endif
    @endforeach
    <td>
        <div class="form-button-action">
            @foreach($actions as $action)
                @if($action == 'view')
                    <a href="{{ route('media-event-view', $event->slug) }}" data-toggle="tooltip" title="View Event"
                        class="btn btn-link btn-success">
                        <i class="fas fa-eye"></i>
                    </a>
                @endif
                @if($action == 'edit')
                    <a href="{{ route('media-event-edit', $event->slug) }}" data-toggle="tooltip" title="Edit Event"
                        class="btn btn-link">
                        <i class="fas fa-edit"></i>
                    </a>
                @endif
                @if($action == 'delete')
                    <form action="{{ route('media-event-delete', $event->slug) }}" method="POST" class="deleteForm">
                        @csrf
                        @method('DELETE')
                        <button type="button" data-toggle="tooltip" title="Delete Event"
                            class="btn btn-link btn-danger deleteBtn">
                            <i class="fas fa-trash"></i>
                        </button>
                    </form>
                @endif
            @endforeach
        </div>
    </td>
</tr>
    