<tr>
    <td>{{ $loop->iteration }}</td>
    @foreach($fields as $field)

        @if($field == 'title')
            <td>{{ $post->title }}</td>
        @endif

        @if($field == 'description')
        <td> {{ str_limit(strip_tags($post->description), 100) }}</td>
        @endif

        @if($field == 'visible')
        <td> {{ $post->visible ? 'Yes' : 'No' }}</td>
        @endif

        @if($field == 'comments')
        <td>{{ number_format($post->comments->count()) }}</td>
        @endif

        @if($field == 'created_by')
        <td>{{ $post->admin->name ?? 'Kainetix Sports' }}</td>
        @endif

        @if($field == 'created_at')
        <td>{{ $post->created_at->format('jS M, Y - h:ia') }}</td>
        @endif

    @endforeach
    <td>
        <div class="form-button-action">
            @foreach($actions as $action)
                @if($action == 'view')
                    <a href="{{ route('media-post-view', $post->slug) }}" data-toggle="tooltip" title="View Post"
                        class="btn btn-link btn-success">
                        <i class="fas fa-eye"></i>
                    </a>
                @endif
                @if($action == 'edit')
                    <a href="{{ route('media-post-edit', $post->slug) }}" data-toggle="tooltip" title="Edit Post"
                        class="btn btn-link">
                        <i class="fas fa-edit"></i>
                    </a>
                @endif
                @if($action == 'delete')
                    <form action="{{ route('media-post-delete', $post->slug) }}" method="POST" class="deleteForm">
                        @csrf
                        @method('DELETE')
                        <button type="button" data-toggle="tooltip" title="Delete Post"
                            class="btn btn-link btn-danger deleteBtn">
                            <i class="fas fa-trash"></i>
                        </button>
                    </form>
                @endif
            @endforeach
        </div>
    </td>
</tr>
    