<tr>
    <td>{{ $loop->iteration }}</td>
    @foreach($fields as $field)
        @if($field == 'name')
        <td><a href="{{ route('view-talent', $talent->slug) }}" data-toggle="tooltip" title="View">{{ $talent->name ?? 'Not Available' }}</a></td>
        @endif
        @if($field == 'email')
        <td>{{ $talent->email ?? 'Not Available' }}</td>
        @endif
        @if($field == 'phone')
        <td>{{ $talent->phone ?? 'Not Available' }}</td>
        @endif
        @if($field == 'sport')
        <td>{{ $talent->sport ?? 'Not Available' }}</td>
        @endif
        @if($field == 'about')
        <td>{{ str_limit(strip_tags($talent->about), 50) }}</td>
        @endif
        @if($field == 'dob')
        <td>{{ $talent->dob ? $talent->dob->format('jS M, Y') : 'Not Available' }}</td>
        @endif
        @if($field == 'avatar')
        <td>
            @if($talent->avatar)
            <img src='{{ asset('img/talents/' . $talent->avatar) }}' width='50'
                class="img-fluid img-thumbnail" />
            @else
            Not Available
            @endif
        </td>
        @endif
        @if($field == 'images')
        <td>{{ number_format(count((array) $talent->images)) }}</td>
        @endif
        @if($field == 'socials')
        <td>{{ number_format(count((array) $talent->socials)) }}</td>
        @endif
        @if($field == 'created_at')
        <td>{{ $talent->created_at->format('jS M, Y - h:ia') }}</td>
        @endif
    @endforeach
    <td>
        <div class="form-button-action">
            @foreach($actions as $action)
                @if($action == 'view')
                    <a href="{{ route('view-talent', $talent->slug) }}"
                        data-toggle="tooltip" title="View"
                        class="btn btn-link btn-success">
                        <i class="fas fa-eye"></i>
                    </a>
                @endif
                @if($action == 'edit')
                    <a href="{{ route('edit-talent', $talent->slug) }}"
                        data-toggle="tooltip" title="Edit" class="btn btn-link">
                        <i class="fas fa-edit"></i>
                    </a>
                @endif
                @if($action == 'delete')
                    <form action="{{ route('delete-talent', $talent->slug) }}"
                        method="POST" class="deleteForm">
                        @csrf
                        @method('DELETE')
                        <button type="button" data-toggle="tooltip" title="Delete"
                            class="btn btn-link btn-danger deleteBtn">
                            <i class="fas fa-trash"></i>
                        </button>
                    </form>
                @endif
            @endforeach
        </div>
    </td>
</tr>
    