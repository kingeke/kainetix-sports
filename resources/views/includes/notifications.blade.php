@if (Session::has('success'))
<input type="hidden" id="success_message" value="{{ Session::pull('success') }}">
<script>
    $(document).ready(function () {
        var message = $('#success_message').val();
        notification(message, 'success');
    });

</script>
@endif
@if (Session::has('error'))
<input type="hidden" id="error_message" value="{{ Session::pull('error') }}">
<script>
    $(document).ready(function () {
        var message = $('#error_message').val();
        notification(message, 'danger');
    });

</script>
@endif
