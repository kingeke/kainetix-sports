<!-- footer -->
<div class="pb-3 mt-5 {{ Request::is('/') ? 'wow fadeInUp' : '' }}" id="footer">
    <div class="container">
        <div class="row mb-5">
            <div class="col-lg-4 mt-5 text-white" id="quick_menu">
                <h4 class="text-uppercase">Quick Menu</h4>
                <div style="width: 100px; height: 5px; background-color: #fff; border-radius: 200px">
                </div>
                <div class="row">
                    <div class="col-4 mt-3">
                        <a href="{{ route('index') }}" class="text-decoration-none text-white">
                            <h5>Home</h5>
                        </a>
                    </div>
                    <div class="col-4 mt-3">
                        <a href="{{ route('about') }}" class="text-decoration-none text-white">
                            <h5>About</h5>
                        </a>
                    </div>
                    <div class="col-4 mt-3">
                        <a href="{{ route('gallery') }}" class="text-decoration-none text-white">
                            <h5>Gallery</h5>
                        </a>
                    </div>
                    <div class="col-4 mt-3">
                        <a href="{{ route('expertise') }}" class="text-decoration-none text-white">
                            <h5>Expertise</h5>
                        </a>
                    </div>
                    <div class="col-4 mt-3">
                        <a href="{{ route('talents') }}" class="text-decoration-none text-white">
                            <h5>Talents</h5>
                        </a>
                    </div>
                    <div class="col-4 mt-3">
                        <a href="{{ route('contact') }}" class="text-decoration-none text-white">
                            <h5>Contact</h5>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mt-5 text-white" id="follow_us">
                <h4 class="text-uppercase">Contact Us</h4>
                <div style="width: 100px; height: 5px; background-color: #fff; border-radius: 200px">
                </div>
                <div class="mt-3">
                    <h6> <i class="fa fa-map-marker-alt fa-500x mt-2"></i> 181 Kofo Abayomi, Victoria Island, Lagos.
                    </h6>
                    <h6> <i class="fa fa-envelope fa-500x mt-2"></i> info@kainetixsports.com</h6>
                    <h6> <i class="fa fa-phone fa-rotate-90 fa-500x mt-2"></i> 08096108151</h6>
                </div>
            </div>
            <div class="col-lg-4 mt-5 text-white">
                <h4 class="text-uppercase">Follow Us</h4>
                <div style="width: 100px; height: 5px; background-color: #fff; border-radius: 200px">
                </div>
                <div class="social">
                    <div class="social-icons mt-4">
                        <a class="inline-block text-white rounded-circle" href="https://twitter.com/KainetixSports"
                            target="_blank">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a class="inline-block text-white rounded-circle ml-5"
                            href="https://www.instagram.com/kainetix_sports/" target="_blank">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center mt-3 text-white">
            &copy; 2019. All Rights Reserved by Kainetix Sport Management
        </div>
    </div>
</div>

<script src="{{ asset('lib/jquery/jquery-ui.js') }}"></script>
<script src="{{ asset('lib/bootstrap/popper.min.js') }}"></script>
<script src="{{ asset('lib/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('lib/jssor-slider/jssor.slider.min.js') }}"></script>
<script src="{{ asset('js/slider.js') }}"></script>
<script src="{{ asset('lib/lightGallery-master/dist/js/lightgallery-all.js') }}"></script>
<script src="{{ asset('lib/wow/wow.min.js') }}"></script>
<script src="{{ asset('lib/parsley/parsley.min.js') }}"></script>
<script src="{{ asset('lib/notify/notify.min.js') }}"></script>
<script src="{{ asset('lib/justifiedGallery/justifiedGallery.min.js') }}"></script>
<script src="{{ asset('lib/owlCarousel/dist/owl.carousel.min.js') }}"></script>
<script src="{{ asset('lib/particles/particles.js') }}"></script>
<script src="{{ asset('js/share.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
</body>

</html>
