<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta property="og:site_name" content="Kainetix Sports">
    <meta property="og:url" content="https://kainetixsports.com">
    <meta property="og:type" content="website">
    <meta property="og:type" content="article">
    <meta property="og:title" content="{{ $post->title ?? 'Kainetix Sports' }}">
    <meta property="og:description"
        content="Kainetix Sports Management is a talent management and sports marketing specialist, taking advantage of the abundance of talents in the African region ">
    <meta property="og:image" content="{{ asset('img/website/logo.jpg') }}">
    <link rel="shortcut icon" href="{{ asset('img/website/favicon.png') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('lib/animate/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/bootstrap/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/lightGallery-master/dist/css/lightgallery.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/lightGallery-master/dist/css/lg-transitions.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/justifiedGallery/justifiedGallery.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/owlCarousel/dist/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/owlCarousel/dist/assets/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/jquery/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5d1c0e195d452b00127d77dd&product='inline-share-buttons' async='async'></script>
    {{-- google recaptcha --}}
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
    <title>Kainetix Sports</title>
</head>

<body>
    @include('includes.notifications')
