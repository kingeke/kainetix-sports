<!-- navbar -->
<nav class="navbar navbar-expand-lg navbar-dark">
    <div class="container">
        <a class="navbar-brand p-0" href="{{ route('index') }}">
            <img src="{{ asset('img/website/logo.jpg') }}" style="width: 150px; height: 100%"
                class="d-inline-block align-top" alt="">
        </a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto text-uppercase">
                <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('index') }}"><i class="fa fa-home"></i></a>
                </li>
                <li class="nav-item {{ Request::is('about') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('about') }}">About</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="media-dropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Media </a>
                    <div class="dropdown-menu text-uppercase" aria-labelledby="media-dropdown">
                        <a class="dropdown-item {{ Request::is('media/events') ? 'active' : '' }}"
                            href="{{ route('events') }}">Events</a>
                        <a class="dropdown-item {{ Request::is('media/gallery') ? 'active' : '' }}"
                            href="{{ route('gallery') }}">Gallery</a>
                        <a class="dropdown-item {{ Request::is('media/news') ? 'active' : '' }}"
                            href="{{ route('posts') }}">News</a>
                        <a class="dropdown-item {{ Request::is('media/videos') ? 'active' : '' }}"
                            href="{{ route('videos') }}">Videos</a>
                    </div>
                </li>
                <li class="nav-item {{ Request::is('expertise') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('expertise') }}">Our Expertise</a>
                </li>
                <li class="nav-item {{ Request::is('talents') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('talents') }}">Our Talents</a>
                </li>
                <li class="nav-item {{ Request::is('contact') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('contact') }}">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
