<div class="container" id="topbar">
    <div class="row">
        <div class="col-sm-6 text-sm-left text-center my-auto">
            <div class="social">
                <div class="social-icons">
                    <a class="inline-block text-white rounded-circle" href="https://twitter.com/KainetixSports"
                        target="_blank">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <a class="inline-block text-white rounded-circle" href="https://www.instagram.com/kainetix_sports/"
                        target="_blank"> <i class="fab fa-instagram"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-6 text-lg-right text-sm-left text-center my-auto">
            <div class="contact">
                <span class="mr-3">
                    <a href="mailto:info@kainetixsports.com?Subject=Enquiry">
                        <i class="fas fa-envelope"></i> info@kainetixsports.com
                    </a>
                </span>
                <br id="contact-breaker">
                <span>
                    <i class="fas fa-phone-square fa-rotate-90"></i> 08096108151
                </span>
            </div>
        </div>
    </div>
</div>
