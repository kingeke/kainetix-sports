@include('master.includes.header')

<div id="about">
    @include('master.includes.topbar')
    @include('master.includes.navbar')
    <div class="centered my-auto mx-auto">
        <p class="text-uppercase">
            about us
        </p>
    </div>
</div>
<div class="container mt-5">
    @if(count($aboutContent) > 0)
    <div class="row">
        @foreach($aboutContent as $about)
        <div class="col-12">
            <div class="title text-uppercase main-text-color mb-3">
                <h4 class="mt-2">{{ $about->title }}</h4>
            </div>
            <hr class="hr">
            {!! $about->description !!}
        </div>
        @endforeach
    </div>
    @else
    <div class="row">
        <div class="col-12">
            <div class="title text-uppercase main-text-color mb-3">
                <h4 class="mt-2">Kainetix Sports</h4>
            </div>
            <hr class="hr">
            <p>
                Kainetix Sports Management is a talent management and sports marketing specialist, taking advantage
                of the abundance of talents in the African region through the following:
                <p>
                    <ul>
                        <li>
                            Provision of sporting consultancy which includes personality branding and management on
                            a global stage for established and budding star athletes.
                        </li>
                        <li> Acquisition of image rights and intellectual property and the global management and
                            marketing of these rights using the most effective communication channels.
                        </li>
                        <li>
                            Event marketing and celebrity management with emphasis on media partnerships,
                            sponsorship and brand extension management.
                        </li>
                        <li>
                            Discovery and management of youth talent for the global and local market.
                        </li>
                    </ul>
                </p>
        </div>
        <div class="col-12">
            <div class="title text-uppercase main-text-color mt-4 mb-3">
                <h4 class="mt-2">Management Team</h4>
            </div>
            <hr class="hr">
            <h4 class="d-inline">Otisi Ukiwe:</h4>
            <p class="d-inline">The principal representative for Kainetix Sports Management is Otisi Ukiwe.
                Otisi is a graduate of University of Kent, UK. Otisi studied business administration and management
                science.
                <br><br>
                Otisi worked in the Corporate Venturing and Business Incubation unit of Nextzon Business Services in
                Lagos, Nigeria.
                Mr. Ukiwe has also worked with International Sports Management Africa, where he was responsible for
                planning and executing several events for charity organizations and the discovery of elite
                soccer/football talent.
            </p>
            <br /><br />
            <h4 class="d-inline">Achike Ofodile:</h4>
            <p class="d-inline">Achike is a registered Intermediary with the Nigerian Football Federation. A law
                graduate from the University of Kent with a masters in Sports Law from Nottingham Law School, he has
                worked in the Sports development sector since 2006. As Vice President (Sport) of Kent Union, he provided
                leadership to Kent Union staff and representatives to ensure sports and other recreational activities
                were effectively and fairly run providing opportunities for all students as well as providing support to
                over 50 sport clubs, working closely with staff and students.
                <br><br>
                Achike’s experience has seen him work with Canterbury City Council and Kent County Council in sports
                development and social empowerment using sports, as well as talent management and development across
                Nigeria.

            </p>
        </div>
    </div>
    @endif
</div>

@include('master.includes.footer')
