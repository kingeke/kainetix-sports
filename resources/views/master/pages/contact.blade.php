@include('master.includes.header')

<div id="contact-bg">
    @include('master.includes.topbar')
    @include('master.includes.navbar')
</div>

<div class="container">
    <div class="row">
        <div class="col-12 text-center mt-5">
            <h2>Contact Us</h2>
            <hr class="hr">
            <p>We'd love to hear from you or answer any questions you have.</p>
        </div>
    </div>
    <div class="row mt-5 mb-5">
        <div class="col-sm-4 col-12 my-auto text-center border-right">
            <h5 class="main-text-color text-uppercase">Address</h5>
            <p>181 Kofo Abayomi Street, Victoria Island.</p>
        </div>
        <div class="col-sm-4 col-12 my-auto text-center border-right">
            <h5 class="main-text-color text-uppercase">Phone Number</h5>
            <p>+234 (0) 809-610-8151</p>
        </div>
        <div class="col-sm-4 col-12 my-auto text-center">
            <h5 class="main-text-color text-uppercase">Email</h5>
            <p><a href="mailto:info@kainetixsports.com">info@kainetixsports.com</a></p>
        </div>
    </div>
</div>

<div class="container" id="contact">
    <form action="{{ route('contact') }}" method="POST" class="pt-5 form">
        @csrf
        <div class="form-row">
            <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" required
                    minlength="4" value="{{ old('name') }}" />
            </div>
            <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" required
                    value="{{ old('email') }}" />
            </div>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required
                minlength="4" value="{{ old('subject') }}" />
        </div>
        <div class="form-group">
            <textarea class="form-control" name="message" rows="5" placeholder="Message" required
                minlength="15">{!! old('message') !!}</textarea>
        </div>
        <div class="text-center mb-3">
            <div class="mb-2 mr-auto">
                <div id="g-recaptcha-v2" class="g-recaptcha" data-sitekey="6LfbOakUAAAAAObroC_jQ_Q9Rw08fukfqJGzpTei"
                    data-callback="recaptchaCallback" data-expired-callback="recaptchaExpired"></div>
            </div>
            <img src="{{ asset('img/website/loader.gif') }}" alt="loader" width="30" class="hidden loader">
            <button type="submit" id="contactButton" class="btn btn-outline-primary formButton" disabled>Send
                Message</button>
        </div>
    </form>
</div>

<script>
    function recaptchaCallback() {
        $('#contactButton').removeAttr('disabled')
    };

    function recaptchaExpired() {
        $('#contactButton').attr('disabled', 'disabled')
    }

</script>

@include('master.includes.footer')
