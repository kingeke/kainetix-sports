@include('master.includes.header')

<div id="expertise">
    @include('master.includes.topbar')
    @include('master.includes.navbar')
</div>
<div class="container mt-5">
    <div id="accordion">
        @if(count($expertiseContent) > 0)
        @foreach($expertiseContent as $expertise)
        <div class="card shadow">
            <div class="card-header main-bg-color" data-toggle="collapse" data-target="#{{$expertise->slug}}">
                <h5 class="mb-0">
                    <span style="cursor: pointer">
                        <div class="title-white text-uppercase text-white">
                            <h4 class="mt-2 ml-3">{{$expertise->title}}</h4>
                        </div>
                    </span>
                </h5>
            </div>
            <div id="{{$expertise->slug}}" class="collapse {{ $loop->iteration == 1 ? 'show' : '' }}"
                data-parent="#accordion">
                <div class="card-body">
                    <div class="row">
                        <div class="{{ $expertise->image ? 'col-md-6 my-auto' : 'col-12' }}">
                            {!! $expertise->description !!}
                        </div>
                        @if($expertise->image)
                        <div class="col-md-6 my-auto">
                            <img src="{{ asset('img/website/expertise/' . $expertise->image) }}" alt="talent-management"
                                class="img-fluid img-thumbnail">
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        @else
        <div class="card shadow">
            <div class="card-header main-bg-color" data-toggle="collapse" data-target="#talentManagement">
                <h5 class="mb-0">
                    <span style="cursor: pointer">
                        <div class="title-white text-uppercase text-white">
                            <h4 class="mt-2 ml-3">Talent Management</h4>
                        </div>
                    </span>
                </h5>
            </div>
            <div id="talentManagement" class="collapse show" data-parent="#accordion">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 my-auto">
                            <p>
                                This involves the discovery and management of careers of exceptional sports talent
                                across the
                                country through a network of scouts, coaches, talent hunt events and agents. The sports
                                include
                                football, basketball and athletics.
                            </p>
                            <p>
                                Talent Management will include the sourcing of professional clubs for the sportsman or
                                sportswoman,
                                sourcing of scholarship opportunities and negotiation of contracts in the best interest
                                of the
                                sports person.
                            </p>
                        </div>
                        <div class="col-md-6 my-auto">
                            <img src="{{ asset('img/website/expertise/talent-management.jpg') }}"
                                alt="talent-management" class="img-fluid img-thumbnail">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card shadow">
            <div class="card-header main-bg-color" data-toggle="collapse" data-target="#sportMedia">
                <h5 class="mb-0">
                    <span style="cursor: pointer">
                        <div class="title-white text-uppercase text-white">
                            <h4 class="mt-2 ml-3">Sports Management</h4>
                        </div>
                    </span>
                </h5>
            </div>
            <div id="sportMedia" class="collapse" data-parent="#accordion">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 my-auto">
                            <p>
                                Development of new sports entertainment television concepts and events and the
                                acquisition of the
                                rights for established television shows and events.
                            </p>
                        </div>
                        <div class="col-md-6 my-auto">
                            <img src="{{ asset('img/website/expertise/sport-media.jpg') }}" alt="sport-media"
                                class="img-fluid img-thumbnail">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card shadow">
            <div class="card-header main-bg-color" id="headingThree" data-toggle="collapse"
                data-target="#sportsConsultancy">
                <h5 class="mb-0">
                    <span style="cursor: pointer">
                        <div class="title-white text-uppercase text-white">
                            <h4 class="mt-2 ml-3">Sports Consultancy</h4>
                        </div>
                    </span>
                </h5>
            </div>
            <div id="sportsConsultancy" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 my-auto">
                            <p>
                                Provision of advice to businesses and government bodies who are interested in getting
                                involved in
                                one area of business development or the other.
                            </p>
                        </div>
                        <div class="col-md-6 my-auto">
                            <img src="{{ asset('img/website/expertise/sport-consultancy.jpg') }}"
                                alt="sport-consultancy" class="img-fluid img-thumbnail">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>



</div>

@include('master.includes.footer')
