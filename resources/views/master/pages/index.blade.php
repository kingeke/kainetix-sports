@include('master.includes.header')

<header>
    <div id="particles-js"></div>
    @include('master.includes.topbar')
    @include('master.includes.navbar')
    <div class="container h-100">
        <div class="row  h-75 my-auto">
            <div class="col-lg-7 col-md-12 my-auto">
                <div class="single-slider-area my-auto">
                    <h1 class="title">Talent Management and <br><span> Sports Marketing Specialist.</span></h1>
                    <p>Event marketing, celebrity management, discovery and management of youth talent!
                    </p>
                    <div class="header-btn-group">
                        <a href="{{ route('contact') }}" class="boxed-btn">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>


<!-- latest videos and events -->
<div class="container">
    <div class="row">
        @if(count($videos) > 0)
        <div class="col-lg-6 mt-5 wow fadeInUp" id="videos">
            <div class="row">
                <div class="col-sm-8 col-6">
                    <div class="title text-uppercase main-text-color">
                        <h4 class="mt-2">Latest Videos</h4>
                    </div>
                </div>
                <div class="col-sm-4 col-6 my-auto text-right">
                    <a href="{{ route('videos') }}"
                        class="btn main-bg-color text-white btn-sm text-uppercase rounded-pill p-2 pl-3 pr-3"><small>All
                            Videos</small></a>
                </div>
            </div>
            <hr class="hr">
            <div class="owl-carousel owl-theme">
                @foreach($videos as $video)
                <div class="item">
                    <div class="card shadow">
                        <iframe width="100%" height="300" src="https://www.youtube.com/embed/{{ $video->youtube_id }}"
                            frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                        <div class="card-header main-bg-color">
                            <h5 class="card-title text-white my-auto">{{ $video->title }}</h5>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endif
        @if(count($events) > 0)
        <div class="col-lg-6 mt-5 wow fadeInUp" id="events">
            <div class="row">
                <div class="col-sm-8 col-6">
                    <div class="title text-uppercase main-text-color">
                        <h4 class="mt-2">Events</h4>
                    </div>
                </div>
                <div class="col-sm-4 col-6 my-auto text-right">
                    <a href="{{ route('events') }}"
                        class="btn main-bg-color text-white btn-sm text-uppercase rounded-pill p-2 pl-3 pr-3"><small>All
                            Events</small></a>
                </div>
            </div>
            <hr class="hr">
            <div class="row">
                <div class="col-12">
                    <table class="table table-hover table-bordered shadow main-bg-color rounded">
                        <tbody>
                            @foreach($events as $event)
                            <tr>
                                <td width="150" class="event-date align-middle text-uppercase text-center">
                                    {{$event->time->format('jS')}} <br> {{$event->time->format('M')}}
                                </td>
                                <td class="align-middle">
                                    <span class="event-title">
                                        <a href="{{ route('events') }}">
                                            {{$event->title}}
                                        </a>
                                        <br>
                                        <small class="text-white">{!! str_limit(strip_tags($event->description), 60)
                                            !!}</small>
                                    </span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endif
        @if(count($posts) > 0)
        <div class="col-lg-12 mt-5 wow fadeInUp" id="news">
            <div class="row">
                <div class="col-sm-8 col-6">
                    <div class="title text-uppercase main-text-color">
                        <h4 class="mt-2">Latest News</h4>
                    </div>
                </div>
                <div class="col-sm-4 col-6 my-auto text-right">
                    <a href="{{ route('posts') }}"
                        class="btn main-bg-color text-white btn-sm text-uppercase rounded-pill p-2 pl-3 pr-3"><small>All
                            News</small></a>
                </div>
            </div>
            <hr class="hr">
            <div class="row">
                @foreach($posts as $post)
                <div class="col-lg-6">
                    <div class="card mb-3 shadow main-bg-color">
                        <div class="row no-gutters">
                            <div class="col-md-5"
                                style="background-image: url('{{ asset(($post->image ? 'img/news/' . $post->image : 'img/defaults/event-default.jpg')) }}'); width: 100%; height: 300px; background-position: center center; background-size: cover">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body">
                                    <a href="{{ route('post', $post->slug) }}" class="text-decoration-none text-white">
                                        <h5 class="card-title">{{ $post->title }}</h5>
                                    </a>
                                    <p class="card-text"><small class="text-white text-uppercase"><i
                                                class="fa fa-clock"></i>
                                            {{ $post->created_at->format('M jS, Y') }}</small></p>
                                    <hr>
                                    <p class="card-text text-white">
                                        {{ str_limit(strip_tags($post->description), 150) }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endif
        @if(count($gallery) > 0)
        <div class="col-lg-6 mt-5  wow fadeInUp">
            <div id="gallery">
                <div class="row">
                    <div class="col-sm-8 col-6">
                        <div class="title text-uppercase main-text-color">
                            <h4 class="mt-2">Gallery</h4>
                        </div>
                    </div>
                    <div class="col-sm-4 col-6 my-auto text-right">
                        <a href="{{ route('gallery') }}"
                            class="btn main-bg-color text-white btn-sm text-uppercase rounded-pill p-2 pl-3 pr-3"><small>View
                                All</small></a>
                    </div>
                </div>
                <hr class="hr">
                <div id="homeGallery">
                    @foreach($gallery as $item)
                    <a class="image" href="{{ asset('img/gallery/' . $item->image) }}"
                        data-tweet-text="{{ $item->title ?? 'Share on Twitter'}}"
                        data-sub-html="<h4>{{ $item->title }}</h4><p>{{ $item->description }}</p>">
                        <img src="{{ asset('img/gallery/' . $item->image) }}" />
                        <div class="view">
                            <i class="fas fa-search"></i>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
        @endif
        <div class="col-lg-6  wow fadeInUp">
            <div id="tweet" class="mt-5">
                <div class="row">
                    <div class="col-12">
                        <div class="title text-uppercase main-text-color">
                            <h4 class="mt-2">Latest Tweet</h4>
                        </div>
                    </div>
                </div>
                <hr class="hr">
                <a class="twitter-timeline" href="https://twitter.com/KainetixSports?ref_src=twsrc%5Etfw"
                    data-height="300" data-chrome="nofooter noheaders noborders">Tweets by
                    KainetixSports</a>
                <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

                <div class="text-center mt-2">
                    <a href="https://twitter.com/KainetixSports?ref_src=twsrc%5Etfw" class="twitter-follow-button"
                        data-show-count="true" data-size="large" data-show-screen-name="true">Follow
                        @KainetixSports</a>
                    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
            </div>
        </div>
    </div>
</div>
@include('master.includes.footer')
