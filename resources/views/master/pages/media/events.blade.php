@include('master.includes.header')

<div id="media">
@include('master.includes.topbar')
@include('master.includes.navbar')
</div>

<div class="container" id="media_events">
    <div class="text-uppercase mt-5">
        <div class="title">
            <h3 class="ml-3">Our Events</h3>
        </div>
        <hr class="hr">
    </div>
    <div class="row mb-5 mt-5">
        @if(count($events) > 0)
        @foreach($events as $event)
        <div class="col-md-6 mt-3 mb-5">
            <div class="card shadow">
                <div class="card-img-top"
                    style="background-image: url('{{ asset(($event->image ? 'img/events/' . $event->image : 'img/defaults/event-default.jpg')) }}'); width: 100%; height: 300px;background-size: cover; background-position: center center">
                </div>
                <div class="card-body">
                    <h5 class="card-title">{{ $event->title  }}</h5>
                    <p class="card-text">
                        {!! str_limit(strip_tags($event->description), 100) !!}
                    </p>
                    <span class="float-right">
                        <button class="btn btn-link" data-toggle="modal" data-target="#{{ $event->slug }}">Read
                            More</button>
                    </span>
                </div>
                <div class="card-footer main-bg-color">
                    <small class="text-white">Event Schedule: {{ $event->time->format('jS M, Y - h:ia')  }}</small>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="{{ $event->slug }}" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-scrollable {{ $date <= $event->time && $event->bookings ? 'modal-xl' : 'modal-lg' }}"
                role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Event - {{ $event->title }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="row">
                                <div
                                    class="{{ $date <= $event->time && $event->bookings ? 'col-lg-8 col-12' : 'col-12' }}">
                                    <div class="row">
                                        <div class="col-sm-6 offset-sm-3 mb-3">
                                            <div class="text-center">
                                                <img src="{{ asset(($event->image ? 'img/events/' . $event->image : 'img/defaults/event-default.jpg')) }}"
                                                    alt="{{ $event->slug }}" class="img-fluid img-thumbnail">
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <p>
                                                {!! $event->description !!}
                                            </p>
                                            <ul>
                                                <li>Venue: {{ $event->venue }}</li>
                                                <li>Schedule: {{ $event->time->format('jS M, Y - h:ia') }} Prompt</li>
                                            </ul>
                                            @if($date <= $event->time)
                                            <div class="col-12 text-center mt-3 mb-5">
                                                <p>Share This Event!</p>
                                                <div class="sharethis-inline-share-buttons"></div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @if($date <= $event->time && $event->bookings)
                                    <div class="col-lg-4 col-12">
                                        <h4>Book Event</h4>
                                        <p>Please fill out the details to book this event.</p>
                                        <form class="form" action="{{ route('events') }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="slug" value="{{ $event->slug }}">
                                            <div class="form-group">
                                                <label>Fullname <span class="text-danger">*</span></label>
                                                <input type="text" name="name" class="form-control"
                                                    placeholder="Enter your name" required minlength="3"
                                                    value="{{ old('name') }}">
                                            </div>
                                            <div class="form-group">
                                                <label>Email address <span class="text-danger">*</span></label>
                                                <input type="email" name="email" class="form-control"
                                                    placeholder="Enter your email" required value="{{ old('email') }}">
                                            </div>
                                            <div class="form-group">
                                                <label>Phone Number <span class="text-danger">*</span></label>
                                                <input type="tel" class="form-control"
                                                    placeholder="Enter your phone number 080xxxxx" name="number"
                                                    required minlength="3" data-parsley-type="digits"
                                                    value="{{ old('number') }}">
                                            </div>
                                            <div class="form-group text-center">
                                                <div class="mb-2 mr-auto">
                                                    <div id="g-recaptcha-v2" class="g-recaptcha"
                                                        data-sitekey="6LfbOakUAAAAAObroC_jQ_Q9Rw08fukfqJGzpTei"
                                                        data-callback="recaptchaCallback"
                                                        data-expired-callback="recaptchaExpired" style=""></div>
                                                </div>
                                                <img src="{{ asset('img/website/loader.gif') }}" alt="loader" width="30"
                                                    class="hidden loader">
                                                <button type="submit" class="btn btn-block btn-primary formButton">Book
                                                    Event</button>
                                            </div>
                                        </form>
                                    </div>
                                    @endif
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        @else
        <div class="col-12">
            <div class="card shadow">
                <div class="card-body text-center">
                    <h4>No events yet, please come back later, Thanks.</h4>
                </div>
            </div>
        </div>
        @endif
    </div>

    {{ $events->links('vendor.pagination.custom') }}

</div>
@include('master.includes.footer')
