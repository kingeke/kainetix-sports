@include('master.includes.header')

<div id="media">
    @include('master.includes.topbar')
    @include('master.includes.navbar')
</div>
<div class="container">
    <div class="text-uppercase mt-5">
        <div class="title">
            <h3 class="ml-3">Our Gallery</h3>
        </div>
        <hr class="hr">
    </div>
    <div class="row mb-5 mt-5">
        @if(count($gallery) > 0)
        <div class="col-12">
            <div id="mediaGallery">
                @foreach($gallery as $item)
                <a class="image" href="{{ asset('img/gallery/' . $item->image) }}"
                    data-tweet-text="{{ $item->title ?? 'Share on Twitter'}}"
                    data-sub-html="<h4>{{ $item->title }}</h4><p>{{ $item->description }}</p>">
                    <img src="{{ asset('img/gallery/' . $item->image) }}" />
                    <div class="view">
                        <i class="fas fa-search"></i>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
        @else
        <div class="col-12">
            <div class="card shadow">
                <div class="card-body text-center">
                    <h4>No pictures yet, please come back later, Thanks.</h4>
                </div>
            </div>
        </div>
        @endif
    </div>

    {{ $gallery->links('vendor.pagination.custom') }}
</div>
@include('master.includes.footer')
