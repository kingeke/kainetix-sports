@include('master.includes.header')

<div id="particles-js"></div>
<div
    style="background-image: url('{{ asset(($post->image ? 'img/news/' . $post->image : 'img/defaults/event-default.jpg')) }}'); width: 100%; height: 600px; background-position: center center; background-size: cover; box-shadow: 0 .5rem 1rem rgba(0, 0, 0, .15) !important; background-attachment: fixed;">
    @include('master.includes.topbar')
    @include('master.includes.navbar')
</div>

<div class="mr-sm-5 ml-sm-5 mt-3 mb-3">
    <div class="row">
        <div class="col-lg-7 mb-3">
            <div class="card shadow">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8 offset-md-2 mb-4">
                            <img src="{{ asset(($post->image ? 'img/news/' . $post->image : 'img/defaults/event-default.jpg')) }}"
                                alt="Post Image" class="img-fluid img-thumbnail">
                        </div>
                        <div class="col-12 text-center">
                            <h4>{{ $post->title }}</h4>
                        </div>
                        <div class="col-12">
                            <p>{!! $post->description !!}</p>
                        </div>
                        <div class="col-12">
                            <p class="text-muted">Published by {{ $post->admin->name ?? 'Kainetix Sports' }} on
                                {{ $post->created_at->format('jS M, Y') }}</p>
                        </div>
                        <div class="col-12 text-center mt-3">
                            <p>Share This Post!</p>
                            <div class="sharethis-inline-share-buttons"></div>

                            {{-- {!!
                            Share::currentPage($post->title)
                            ->facebook()
                            ->twitter()
                            ->linkedin(str_limit(strip_tags($post->description), 200))
                            ->whatsapp()
                            !!} --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-5">
            <div class="card shadow">
                <div class="card-header main-bg-color text-white">
                    <div class="row">
                        <div class="col-sm-6 text-sm-left text-center my-auto">
                            <h6 class="">Comments <span
                                    class="badge badge-light">{{ number_format($comments->count()) }}</span></h6>
                        </div>
                        <div class="col-sm-6 text-sm-right text-center">
                            <button class="btn btn-outline-light btn-sm" data-toggle="modal"
                                data-target="#commentModal">Leave a
                                comment</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row" style="max-height: 550px; overflow-y: scroll">
                        @if(count($comments) > 0)
                        @foreach($comments->where('parent_id', null) as $comment)
                        <div class="col-12">
                            <div class="row">
                                <div class="col-lg-2 col-sm-1 text-center">
                                    <img src="{{ asset('img/defaults/user-default.jpg') }}" class="rounded-circle"
                                        width="40">
                                </div>
                                <div class="col-lg-10 col-sm-11">
                                    <div class="text-sm-left text-center">
                                        <p class="text-dark m-0" style="font-size: 13px">{{ $comment->name }}
                                            <span class="text-muted">{{ $comment->created_at->diffForHumans() }}</span>
                                        </p>
                                    </div>
                                    <p class="m-0 mt-1 ml-sm-0 ml-2" style="font-size: 14px">{{ $comment->comment }}</p>

                                    <div class="text-right mr-2">
                                        <button class="btn btn-sm btn-outline-primary replyButton mt-2"
                                            data-id="{{ $comment->id }}" data-toggle="modal" data-target="#commentModal"
                                            data-child={{ $comment->id }}>Reply</button>
                                    </div>
                                </div>
                                @if($comment->replies->count() > 0)
                                <div class="col-lg-10 col-sm-11 offset-lg-2 offset-sm-1 mb-3 text-sm-left text-center">
                                    <p class="m-0 mt-1 font-weight-bold replies"
                                        style="font-size: 14px; cursor: pointer" data-toggle="collapse"
                                        href="#replyCollapse{{ $comment->id }}"><span>View
                                            {{ $comment->replies->count() == 1 ? 'Reply' : number_format($comment->replies->count()) . ' Replies'}}</span>
                                    </p>
                                </div>
                                @foreach($comment->replies as $reply)
                                <div class="col-12 mb-2 collapse" id="replyCollapse{{ $comment->id }}">
                                    <div class="row">
                                        <div class="offset-1"></div>
                                        <div class="col-lg-2 col-sm-1 text-center">
                                            <img src="{{ asset('img/defaults/user-default.jpg') }}"
                                                class="rounded-circle" width="40">
                                        </div>
                                        <div class="col-lg-9 col-sm-10">
                                            <div class="text-sm-left text-center">
                                                <p class="text-dark m-0" style="font-size: 13px">{{ $reply->name }}
                                                    <span
                                                        class="text-muted">{{ $reply->created_at->diffForHumans() }}</span>
                                                </p>
                                            </div>
                                            <p class="m-0 mt-1" style="font-size: 14px"> <span
                                                    class="text-primary">{{ '@' }}{{ $reply->user->name ?? 'Kainetix Sports' }}</span>
                                                {{ $reply->comment }}</p>

                                            <div class="text-right mr-2">
                                                <button class="btn btn-sm btn-outline-primary mt-2 replyButton"
                                                    data-id="{{ $comment->id }}" data-child="{{ $reply->id }}"
                                                    data-toggle="modal" data-target="#commentModal">Reply</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @endif
                            </div>
                        </div>
                        @endforeach
                        @else
                        <div class="col-12 text-center">
                            <p>Be the first to leave a comment <i class="fas fa-smile-wink text-primary"></i></p>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Comment Modal -->
<div class="modal fade" id="commentModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Comment on <small>{{ $post->title }}</small></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="container">
                <form class="form" action="{{ route('post', $post->slug) }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="comment_id" id="comment_id" value="">
                        <input type="hidden" name="child_id" id="child_id" value="">
                        <div class="row">
                            @include('admin.layouts.inputs',[
                            'inputType' => 'textBox',
                            'label' => 'Name',
                            'required' => true,
                            'name' => 'name',
                            ])

                            @include('admin.layouts.inputs',[
                            'inputType' => 'textBox',
                            'label' => 'Email',
                            'required' => true,
                            'type' => 'email',
                            'name' => 'email',
                            ])

                            @include('admin.layouts.inputs',[
                            'inputType' => 'textarea',
                            'label' => 'Comment',
                            'required' => true,
                            'name' => 'comment',
                            'col' => 'col-12'
                            ])
                            <div class="col-12">
                                <div class="form-group text-center">
                                    <div class="mb-2 mr-auto">
                                        <div id="g-recaptcha-v2" class="g-recaptcha"
                                            data-sitekey="6LfbOakUAAAAAObroC_jQ_Q9Rw08fukfqJGzpTei"
                                            data-callback="recaptchaCallback" data-expired-callback="recaptchaExpired"
                                            style="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <img src="{{ asset('img/website/loader.gif') }}" alt="loader" width="30" class="hidden loader">
                        <button type="submit" class="btn btn-primary formButton">Submit Comment</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('.replyButton').each(function () {
            $(this).click(function () {
                $('#comment_id').val($(this).data('id'))
                $('#child_id').val($(this).data('child'))
            })
        })
    });

</script>

@include('master.includes.footer')
