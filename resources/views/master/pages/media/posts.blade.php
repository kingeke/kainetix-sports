@include('master.includes.header')

<div id="media">
    @include('master.includes.topbar')
    @include('master.includes.navbar')
</div>

<div class="container">
    <div class="text-uppercase mt-5">
        <div class="title">
            <h3 class="ml-3">Latest Posts</h3>
        </div>
        <hr class="hr">
    </div>
    @if(count($posts) > 0)
    @foreach($posts as $post)
    <div class="row mb-5 mt-5">
        <div class="col-md-4 mb-5"
            style="background-image: url('{{ asset(($post->image ? 'img/news/' . $post->image : 'img/defaults/event-default.jpg')) }}'); width: 100%; height: 250px; background-position: center center; background-size: cover; border-radius: .25rem; box-shadow: 0 .5rem 1rem rgba(0, 0, 0, .15) !important;">
        </div>
        <div class="col-md-7 mx-auto">
            <a href="{{ route('post', $post->slug) }}" class="text-dark text-decoration-none">
                <h4>{{ $post->title }}</h4>
            </a>
            <p>{{ str_limit(strip_tags($post->description), 500) }}</p>
            @if($post->comments->count() > 0)
            <p><small class="text-muted">{{ number_format($post->comments->count()) }} Comments</small></p>
            @endif
            <div class="row">
                <div class="col-md-8">
                    <p><small class="text-muted">Published by {{ $post->admin->name ?? 'Kainetix Sports' }} on
                            {{ $post->created_at->format('jS M, Y') }}</small></p>
                </div>
                <div class="col-md-4 text-md-right">
                    <a href="{{ route('post', $post->slug) }}" class="btn btn-sm btn-outline-primary">Read More</a>
                </div>
            </div>
        </div>
    </div>
    <hr class="hr" style="max-width: 50%;">
    @endforeach
    @else
    <div class="row mb-5 mt-5">
        <div class="col-12">
            <div class="card shadow">
                <div class="card-body text-center">
                    <h4>No posts yet, please come back later, Thanks.</h4>
                </div>
            </div>
        </div>
    </div>
    @endif

    {{ $posts->links('vendor.pagination.custom') }}
</div>
@include('master.includes.footer')
