@include('master.includes.header')

<div id="media">
    @include('master.includes.topbar')
    @include('master.includes.navbar')
</div>

<div class="container">
    <div class="text-uppercase mt-5">
        <div class="title">
            <h3 class="ml-3">Our Videos</h3>
        </div>
        <hr class="hr">
    </div>
    <div class="row mt-5 mb-5">
        @if(count($videos) > 0)
        @foreach ($videos as $video)
        <div class="col-lg-6 mb-5">
            <div class="card shadow">
                <iframe width="100%" height="300" src="https://www.youtube.com/embed/{{ $video->youtube_id }}"
                    frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
                <div class="card-header main-bg-color">
                    <h5 class="card-title text-white my-auto">{{ $video->title }}</h5>
                </div>
                @if($video->description)
                <div class="card-body">
                    <p> {{ str_limit($video->description, 200) }} </p>
                    @if(strlen($video->description) > 200)
                    <button class="btn btn-outline-primary readMore" data-description="{{ $video->description }}"
                        data-title="{{ $video->title }}" data-toggle="modal" data-target="#videoModal">Read
                        More</button>
                    @endif
                </div>
                @endif
            </div>
        </div>
        @endforeach
        @else
        <div class="col-12">
            <div class="card shadow">
                <div class="card-body text-center">
                    <h4>No videos yet, please come back later, Thanks.</h4>
                </div>
            </div>
        </div>
        @endif
    </div>

    {{ $videos->links('vendor.pagination.custom') }}
</div>

<!-- Modal -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="videoTitle"></h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="videoDescription"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('.readMore').each(function () {
            $(this).click(function () {
                $('#videoDescription').html($(this).data('description'))
                $('#videoTitle').html($(this).data('title'))
            })
        })
    });

</script>

@include('master.includes.footer')
