@include('master.includes.header')

<div id="talents">
    @include('master.includes.topbar')
    @include('master.includes.navbar')
    <div class="centered">
        <p class="text-uppercase">
            Our Talents
        </p>
    </div>
</div>

<div class="container talents">
    <div class="row mt-5 mb-5">
        @if(count($talents) > 0)
        @foreach($talents as $talent)
        <div class="col-lg-6 mb-5">
            <div class="card shadow">
                <div class="talent-img"
                    style="background-image: url('{{ asset(($talent->avatar ? 'img/talents/' . $talent->avatar : 'img/defaults/talent-default.jpg')) }}'); width: 100%; height: 300px;background-size: cover; background-position: center center;">
                </div>
                <div class="card-body">
                    <h5 class="card-title">{{ $talent->name }}</h5>
                    <p class="card-text">{!! str_limit(strip_tags($talent->about), 200) !!}</p>
                    <button type="button" class="btn btn-outline-primary shadow" data-toggle="modal"
                        data-target="#talent-modal-{{ $talent->slug }}">Read More</button>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="talent-modal-{{ $talent->slug }}" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                <div class="modal-content container">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ $talent->name }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 mb-3">
                                <p>
                                    {!! $talent->about !!}
                                </p>
                                @if($talent->sport)
                                <p><b>Sport:</b> {{ $talent->sport }}</p>
                                @endif
                                @if($talent->dob)
                                <p><b>DOB:</b> {{ $talent->dob->format('jS M, Y') }}</p>
                                @endif
                                @if(count((array) $talent->socials) > 0)
                                <div id="talent">
                                    <div class="social-icons">
                                        @foreach($talent->socials as $social => $link)
                                        <a class="inline-block text-white rounded-circle" href="{{ $link }}"
                                            target="_blank">
                                            @if($social == 'facebook')
                                            <i class="fab fa-facebook-f"></i>
                                            @elseif($social == 'linkedIn')
                                            <i class="fab fa-linkedin-in"></i>
                                            @elseif($social == 'twitter')
                                            <i class="fab fa-twitter"></i>
                                            @elseif($social == 'instagram')
                                            <i class="fab fa-instagram"></i>
                                            @endif
                                        </a>
                                        @endforeach
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="col-lg-6 mb-3">
                                <img src="{{ asset(($talent->avatar ? 'img/talents/' . $talent->avatar : 'img/defaults/talent-default.jpg')) }}"
                                    alt="{{ $talent->slug }}" class="img-fluid img-thumbnail shadow">
                            </div>
                            <div class="col-lg-6">
                                <div class="owl-carousel talent-carousel owl-theme talentGallery">
                                    @if(count((array) $talent->images) > 0)
                                    @foreach($talent->images as $image)
                                    <div class="col-12">
                                        <a class="image" href="{{ asset('img/talents/' . $image) }}" target="_blank"
                                            data-tweet-text="Hey guys, meet {{ $talent->name }} on Kainetix Sports">
                                            <img src="{{ asset('img/talents/' . $image) }}"
                                                class="img-fluid img-thumbnail" />
                                            <div class="view">
                                                <i class="fas fa-search"></i>
                                            </div>
                                        </a>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        @else
        <div class="col-12">
            <div class="card shadow">
                <div class="card-body text-center">
                    <h4>We're still scouting for talented players, please come back later, Thanks.</h4>
                </div>
            </div>
        </div>
        @endif
    </div>
    {{ $talents->links('vendor.pagination.custom') }}
</div>

@include('master.includes.footer')
