<?php

//index
Route::get('/', ['uses' => 'WebsiteController@index', 'as' => 'index']);

//about page
Route::get('/about', ['uses' => 'WebsiteController@about', 'as' => 'about']);

//expertise page
Route::get('/expertise', ['uses' => 'WebsiteController@expertise', 'as' => 'expertise']);

//contact page
Route::match(['get', 'post'], '/contact',  ['uses' => 'WebsiteController@contact', 'as' => 'contact']);

//videos page
Route::get('/media/videos', ['uses' => 'WebsiteController@videos', 'as' => 'videos']);

//events page
Route::match(['get', 'post'], '/media/events', ['uses' => 'WebsiteController@events', 'as' => 'events']);

//posts page
Route::get('/media/news', ['uses' => 'WebsiteController@posts', 'as' => 'posts']);

//post
Route::match(['get', 'post'], '/media/news/{slug}', ['uses' => 'WebsiteController@post', 'as' => 'post']);

//gallery page
Route::get('/media/gallery', ['uses' => 'WebsiteController@gallery', 'as' => 'gallery']);

//talents page
Route::get('/talents', ['uses' => 'WebsiteController@talents', 'as' => 'talents']);

//admin login page
Route::match(['get', 'post'], '/admin/login', ['uses' => 'AdminController@login', 'as' => 'login']);

//admin logout
Route::post('/admin/logout', ['uses' => 'AdminController@logout', 'as' => 'logout']);

//password resets

//send password reset link
Route::match(['get', 'post'], '/admin/login/forgot-password', ['uses' => 'AdminController@forgotPassword', 'as' => 'forgot-password']);

//show the url
Route::get('/admin/login/reset-password/{token}', ['uses' => 'AdminController@passwordReset', 'as' => 'password-reset']);

//reset the password and save to db
Route::post('admin/login/reset-password', ['uses' => 'Auth\ResetPasswordController@reset', 'as' => 'password-reset-post']);

require base_path('routes/web/admin.php');