<?php

//admin routes
Route::group(['prefix' => 'admin', 'middleware' => 'auth:admins'],function(){

    //index
    Route::get('/', ['uses' => 'AdminController@index', 'as' => 'admin-dashboard']);

    //uploading images from summer api
    Route::post('/upload-image' , ['uses' => 'AdminController@summerUploader']);

    //website
    require base_path('routes/web/adminRoutes/website.php');

    //admins
    require base_path('routes/web/adminRoutes/admins.php');

    //media
    require base_path('routes/web/adminRoutes/media.php'); 

    //talents
    require base_path('routes/web/adminRoutes/talents.php');
});