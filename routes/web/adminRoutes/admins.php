<?php

//create
Route::match(['get', 'post'], 'authorized-admin/create',  ['uses' => 'AdminController@createAdmin', 'as' => 'create-admin']);

Route::group(['prefix' => 'authorized-admins'],function(){

    //view
    Route::get('/', ['uses' => 'AdminController@viewAdmins', 'as' => 'view-admins']);
    
    //edit
    Route::match(['get', 'put'], '/{slug}/edit/{type}',  ['uses' => 'AdminController@editAdmin', 'as' => 'edit-admin']);

    //delete
    Route::delete('/{slug}/delete', ['uses' => 'AdminController@deleteAdmin', 'as' => 'delete-admin']);

});
