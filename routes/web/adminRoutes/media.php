<?php

Route::group(['prefix' => 'media'],function(){
    
    //events
    require base_path('routes/web/adminRoutes/mediaRoutes/events.php');

    //gallery
    require base_path('routes/web/adminRoutes/mediaRoutes/gallery.php');
    
    //posts
    require base_path('routes/web/adminRoutes/mediaRoutes/posts.php');
    
    //videos
    require base_path('routes/web/adminRoutes/mediaRoutes/videos.php');
});
