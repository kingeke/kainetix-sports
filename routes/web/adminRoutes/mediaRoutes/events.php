<?php

//create event
Route::match(['get', 'post'], '/events/create',  ['uses' => 'AdminController@createEvent', 'as' => 'media-event-create']);

//view events
Route::get('/events', ['uses' => 'AdminController@viewEvents', 'as' => 'media-events-view']);

//view event
Route::get('/events/{slug}', ['uses' => 'AdminController@viewEvent', 'as' => 'media-event-view']);

//edit event
Route::match(['get', 'put'], '/events/{slug}/edit',  ['uses' => 'AdminController@editEvent', 'as' => 'media-event-edit']);

//delete event
Route::delete('/events/{slug}/delete', ['uses' => 'AdminController@deleteEvent', 'as' => 'media-event-delete']);