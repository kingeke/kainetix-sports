<?php

//create gallery
Route::match(['get', 'post'], '/gallery/create',  ['uses' => 'AdminController@createGallery', 'as' => 'media-gallery-create']);

//view gallery
Route::get('/gallery', ['uses' => 'AdminController@viewGallery', 'as' => 'media-gallery-view']);

//edit gallery
Route::match(['get', 'put'], '/gallery/{slug}/edit',  ['uses' => 'AdminController@editGallery', 'as' => 'media-gallery-edit']);

//delete gallery
Route::delete('/gallery/{slug}/delete', ['uses' => 'AdminController@deleteGallery', 'as' => 'media-gallery-delete']);