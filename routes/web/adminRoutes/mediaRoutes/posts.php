<?php

//create post
Route::match(['get', 'post'], '/posts/create',  ['uses' => 'AdminController@createPost', 'as' => 'media-post-create']);

//view posts
Route::get('/posts', ['uses' => 'AdminController@viewPosts', 'as' => 'media-posts-view']);

//view post
Route::get('/posts/{slug}', ['uses' => 'AdminController@viewPost', 'as' => 'media-post-view']);

//edit post
Route::match(['get', 'put'], '/posts/{slug}/edit',  ['uses' => 'AdminController@editPost', 'as' => 'media-post-edit']);

//delete post
Route::delete('/posts/{slug}/delete', ['uses' => 'AdminController@deletePost', 'as' => 'media-post-delete']);

//delete post comment
Route::delete('/comment/{id}/delete', ['uses' => 'AdminController@deleteComment', 'as' => 'media-post-delete-comment']);