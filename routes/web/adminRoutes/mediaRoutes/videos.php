<?php

//create video
Route::match(['get', 'post'], '/videos/create',  ['uses' => 'AdminController@createVideo', 'as' => 'media-video-create']);

//view videos
Route::get('/videos', ['uses' => 'AdminController@viewVideos', 'as' => 'media-videos-view']);

//edit video
Route::match(['get', 'put'], '/videos/{slug}/edit',  ['uses' => 'AdminController@editVideo', 'as' => 'media-video-edit']);

//delete video
Route::delete('/videos/{slug}/delete', ['uses' => 'AdminController@deleteVideo', 'as' => 'media-video-delete']);