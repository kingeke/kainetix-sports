<?php


//create talent
Route::match(['get', 'post'], 'talent/create',  ['uses' => 'AdminController@createTalent', 'as' => 'create-talent']);

Route::group(['prefix' => 'talents'],function(){

    //view talents
    Route::get('/', ['uses' => 'AdminController@viewTalents', 'as' => 'view-talents']);

    //view talent
    Route::get('/{slug}', ['uses' => 'AdminController@viewTalent', 'as' => 'view-talent']);
    
    //edit talent
    Route::match(['get', 'put'], '/{slug}/edit',  ['uses' => 'AdminController@editTalent', 'as' => 'edit-talent']);

    //delete talent
    Route::delete('/{slug}/delete', ['uses' => 'AdminController@deleteTalent', 'as' => 'delete-talent']);

    //delete images
    Route::post('/{slug}/delete-image', ['uses' => 'AdminController@deleteImage', 'as' => 'delete-image']);

});
