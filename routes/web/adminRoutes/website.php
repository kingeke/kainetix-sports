<?php

Route::group(['prefix' => 'website'],function(){
    
    //about
    //create
    Route::match(['get', 'post'], '/about/create',  ['uses' => 'AdminController@createAbout', 'as' => 'website-about-create']);

    //view
    Route::get('/about', ['uses' => 'AdminController@viewAbouts', 'as' => 'website-about-view']);
    
    //edit
    Route::match(['get', 'put'], '/about/{slug}/edit',  ['uses' => 'AdminController@editAbout', 'as' => 'website-about-edit']);

    //delete
    Route::delete('/about/{slug}/delete', ['uses' => 'AdminController@deleteAbout', 'as' => 'website-about-delete']);

    //expertise
    //create
    Route::match(['get', 'post'], '/expertise/create',  ['uses' => 'AdminController@createExpertise', 'as' => 'website-expertise-create']);

    //view
    Route::get('/expertise', ['uses' => 'AdminController@viewExpertises', 'as' => 'website-expertise-view']);
    
    //edit
    Route::match(['get', 'put'], '/expertise/{slug}/edit',  ['uses' => 'AdminController@editExpertise', 'as' => 'website-expertise-edit']);

    //delete
    Route::delete('/expertise/{slug}/delete', ['uses' => 'AdminController@deleteExpertise', 'as' => 'website-expertise-delete']);
});
